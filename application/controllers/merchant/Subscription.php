<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_nofollow'] = true;
		$this->load->library('merchant_lib');
		$this->load->library('subscription_lib');
		$this->load->model('subscription_type_model');
		$this->load->model('merchant_pending_subscription_model');
		$this->load->model('merchant_subscription_model');
	}

	public function index() {
		$this->page_criteria('active');
		$id_merchant = $this->get_merchant_session_id();
		$this->data['title'] = "AntrianQue - Paket Langganan";
		$this->data['meta_description'] = "Login sekarang dan kontrol antrian di tempat usaha anda menggunakan teknologi internet.";
		$this->data['custom_js'] = array('merchant/subscription/subscription');
		$this->data['merchant_data'] = $this->merchant_lib->get_merchant_data();
		$this->data['subscriptions'] = $this->subscription_type_model->get();
		$this->data['merchant_subscription'] = $this->merchant_subscription_model->get_merchant_active_subscription($id_merchant);
		$this->data['pending_subscription'] = $this->merchant_pending_subscription_model->get_merchant_pending_subscription($id_merchant);
		$this->data['pending_subscription'] = reset($this->data['pending_subscription']);
		foreach ($this->data['subscriptions'] as $key => $value) {
			$price = $this->data['subscriptions'][$key]['price'];
			$discount = $this->data['subscriptions'][$key]['discount'];
			$discount_calculated = $price * $discount / 100;
			$price_after_discount = $price - $discount_calculated;
			$this->data['subscriptions'][$key]['price_after_discount'] = $price_after_discount;
		}
		$this->page_display->display_merchant('subscription/index',$this->data);
	}

	public function buy($id_subscription) {
		$this->page_criteria('active');
		$id_merchant = $this->get_merchant_session_id();
		$subscription = $this->subscription_type_model->get($id_subscription);
		$subscription = reset($subscription);
		if ($subscription){
			$this->data['title'] = "AntrianQue - ".$subscription['name'];
			$this->data['meta_nofollow'] = true;
			$this->data['custom_js'] = array('merchant/subscription/subscription');
			$this->data['merchant_data'] = $this->merchant_lib->get_merchant_data();
			$this->data['subscription'] = $subscription;
			$this->data['fee'] = $this->subscription_lib->get_subscription_fee($this->data['subscription']['price'],$this->data['subscription']['discount']);
			$this->data['subscription']['total'] = $this->subscription_lib->get_total($this->data['subscription']['price'],$this->data['subscription']['discount']);
			$active_subscription = $this->merchant_subscription_model->get_merchant_active_subscription($id_merchant);
			if ($active_subscription && $active_subscription['id_subscription_type'] == $id_subscription){
				$this->data['label'] = "Diperpanjang Sampai";
				$this->data['subscription_expired'] = local('d-m-Y', strtotime('+ '.$subscription['duration'].' days', strtotime($active_subscription['subscription_expired'])));
			} else {
				$this->data['label'] = "Berakhir Pada";
				$this->data['subscription_expired'] = local('d-m-Y', strtotime('+ '.$subscription['duration'].' days', strtotime(date('Y-m-d H:i:s'))));
			}
			$this->page_display->display_merchant('subscription/buy',$this->data);
		} else {
			redirect('merchant/subscription');
		}
	}

	public function delete() {
		$result['status'] = 'failed';
		if ($this->is_loged_merchant() && $this->input->post('method') == 'delete'){
			$id_merchant = $this->get_merchant_session_id();
			$is_deleted = $this->subscription_lib->disable_merchant_active_subscription($id_merchant, 1);
			if ($is_deleted) $result['status'] = 'success';
		}
		echo json_encode($result);
	}

	public function do_payment() {
		$result['status'] = 'failed';
		if ($this->input->post('id_subscription') && $this->is_loged_merchant()){
			$id_subscription = $this->input->post('id_subscription');
			$id_merchant = $this->get_merchant_session_id();
			$merchant = $this->merchant_lib->get_merchant_data($id_merchant);
			$data_subscription = $this->subscription_type_model->get($id_subscription);
			$data_subscription = reset($data_subscription);
			$data_subscription['total_price'] = $this->subscription_lib->get_total($data_subscription['price'],$data_subscription['discount']);
			if ($data_subscription){
				$header = array("Content-Type: application/x-www-form-urlencoded");
		        $body_curl = $this->generate_criteria_api($data_subscription,$merchant);
		        $get_api = $this->do_curl(IPAYMU_API_PAYMENT_URL, $header, $body_curl, $method = "POST");
		        $response = json_decode($get_api['data'],true);
		        $status = json_decode($get_api['code']);
		        if ($response && $status == 200){
		        	$url_redirect = $response['url'];
		        	$criteria_pending_subscription = array(
		        		'id_merchant' => $id_merchant,
		        		'id_subscription_type' => $data_subscription['id_subscription_type'],
		        		'subscription_name' => $data_subscription['name'],
		        		'duration' => $data_subscription['duration'],
		        		'max' => $data_subscription['max'],
		        		'price' => $data_subscription['total_price'],
		        		'id_api_session' => $response['sessionID'],
		        	);
		        	$this->subscription_lib->save_subscription_pending($criteria_pending_subscription);
		        	$result['status'] = 'success';
		        	$result['url'] = $url_redirect;
		        }
			}
		}
		echo json_encode($result);
	}

	public function response() {
		if ($_SERVER['REMOTE_ADDR'] == IPAYMU_API_RESPONSE_IP && $this->input->post()){
			$data = $this->input->post();
			$session_id = $data['sid'];
			$trx_id = $data['trx_id'];
			$status = $data['status'];
			$via = $data['via'];

			$pending_subscription = $this->merchant_pending_subscription_model->get(array('id_api_session' => $session_id));
			$pending_subscription = reset($pending_subscription);
			if ($pending_subscription && $pending_subscription['id_merchant']){
				$id_merchant = $pending_subscription['id_merchant'];
				$header = array("Content-Type: application/x-www-form-urlencoded");
		        $criteria_check_transaction = $this->generate_criteria_check_transaction_api($trx_id);
		        $get_api = $this->do_curl(IPAYMU_API_CHECK_URL, $header, $criteria_check_transaction, $method = "POST");
		        $transaction = json_decode($get_api['data'],true);
		        $status_api_transaction = json_decode($get_api['code'],true);
		        if ($transaction && $status_api_transaction == 200){
		        	if ($status == STATUS_PAYMENT_SUCCESS && $transaction['Status'] == 1){
		        		$active_subscription = $this->merchant_subscription_model->get_merchant_active_subscription($id_merchant);
		        		$new_subscription_start = date('Y-m-d');
		        		if ($active_subscription){
		        			if ($active_subscription['id_subscription_type'] == $pending_subscription['id_subscription_type']){
		        				$new_subscription_start = $active_subscription['subscription_expired'];
		        			}
		        			$is_deleted = $this->subscription_lib->disable_merchant_active_subscription($id_merchant, 0);
		        			if (!$is_deleted){
		        				header("HTTP/1.1 503 Internal Server Error");
		        				die("Internal server error");
		        			}
		        		}
		        		$pending_subscription['subscription_start'] = $new_subscription_start;
		        		$pending_subscription['trx_id'] = $trx_id;
		        		$criteria_merchant_subscription = $this->generate_criteria_merchant_subscription($pending_subscription, $transaction);
		        		if ($this->merchant_subscription_model->insert($criteria_merchant_subscription)){
		        			if ($this->merchant_pending_subscription_model->delete($pending_subscription['id_merchant_pending_subscription'])){
		        				$criteria_email = $criteria_merchant_subscription;
			        			$merchant_data = $this->merchant_lib->get_merchant_data($id_merchant);
			        			$criteria_email['email'] = $merchant_data['email'];
			        			$criteria_email['paid_date'] = $transaction['Waktu'];
			        			$criteria_email['price'] = number_format($pending_subscription['price'],false,',','.');
			        			$criteria_email['subscription_start'] = date("d/m/Y", strtotime($criteria_email['subscription_start']));
			        			$criteria_email['subscription_expired'] = date("d/m/Y", strtotime($criteria_email['subscription_expired']));
			        			$criteria_email['duration'] = $pending_subscription['duration'];
			        			$criteria_email['max'] = number_format($criteria_email['max'],false,",",".");
			        			$this->email_lib->do_send_subscription_payment_successful($criteria_email);
			        			header("HTTP/1.1 200 ok");
			        			die("Send response success");
		        			} else {
		        				header("HTTP/1.1 503 Internal Server Error");
		        				die("Internal server error");
		        			}
		        		} else {
		        			header("HTTP/1.1 503 Internal Server Error");
	        				die("Internal server error");
		        		}
		        	}
					header("HTTP/1.1 403 Forbidden");
					die("Payment not successful yet");
		        } else {
		        	header("HTTP/1.1 503 Unavailable");
		        	die("Cannot get transaction from payment gateway");
		        }
			} else {
				header("HTTP/1.1 503 Unavailable");
				die("Cannot get saved pending subscription");
			}
		} else {
			header("HTTP/1.1 403 Forbidden");
			die("Who the hell are you? You are not allowed");
		}
	}

	private function generate_criteria_api($data_subscription,$merchant){
		$criteria = array(
            'key' => $this->config->item('credential_ipaymu_key'),
            'action' => 'payment',
            'product' => 'AntrianQue '.$data_subscription['name'],
            'price' => $data_subscription['total_price'],
            'quantity' => 1,
            'expired' => 6,
            'comments' => "Pembayaran untuk paket langganan AntrianQue. Durasi ".$data_subscription['duration']." hari",
            'ureturn' => base_url('merchant/subscription').'?success=true',
            'unotify' => base_url('merchant/subscription/response'),
            'ucancel' => base_url('merchant/subscription').'?success=true',
            'buyer_name' => $merchant['name'],
            'buyer_phone' => false,
            'buyer_email' => $merchant['email'],
            'format' => 'json'
        );
        return $criteria;
	}

	private function generate_criteria_check_transaction_api($trx_id) {
		$criteria = array(
			'key' => $this->config->item('credential_ipaymu_key'),
			'id' => $trx_id,
            'format' => 'json'
		);
		return $criteria;
	}

	private function generate_criteria_merchant_subscription($subscription, $transaction) {
		$duration = $subscription['duration'];
		$subscription_start = $subscription['subscription_start'];
		$subscription_expired = date('Y-m-d', strtotime('+ '.$duration.' days', strtotime($subscription_start)));
		$criteria = array(
			'id_merchant' => $subscription['id_merchant'],
			'subscription_name' => $subscription['subscription_name'],
			'subscription_start' => date('Y-m-d'),
			'subscription_expired' => $subscription_expired,
			'id_subscription_type' => $subscription['id_subscription_type'],
			'id_payment' => $subscription['trx_id'],
			'price' => $subscription['price'],
			'paid_date' => date('Y-m-d'),
			'max' => $subscription['max']
		);
		return $criteria;
	}
}