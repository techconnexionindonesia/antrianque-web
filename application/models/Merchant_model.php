<?php

class Merchant_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= MERCHANT_TABLE;
    protected $_primary_key = MERCHANT_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}