<div class="container-fluid">
	<div class="space" style="height: 80px"></div>
	<div class="col-10 mx-auto">
		<span class="text-secondary"><h4>Paket yang sedang digunakan</h4></span>
		<hr>
		<div class="row justify-content-between">
			<div class="col-lg-2 mb-5">
				<div class="row h-100">
					<i class="display-1 fas fa-desktop text-secondary m-auto"></i>
				</div>
			</div>
			<div class="col-lg-7 bg-light rounded p-3 mb-5">
				<table class="w-100">
					<tr>
						<td class="subscription-info-table">
							<h5><i class="fas fa-shopping-cart mr-2"></i>Nama Paket</h5>
							<div class="d-block d-lg-none">
								<h5><span class="mr-2"></span><b><?=$merchant_subscription['subscription_name'] ? $merchant_subscription['subscription_name'] : 'Paket Gratis'?></b></h5>
								<hr>
							</div>
						</td>
						<td class="d-none d-lg-block"><h5>: <b><?=$merchant_subscription['subscription_name'] ? $merchant_subscription['subscription_name'] : 'Paket Gratis'?></b></h5></td>
					</tr>
					<tr>
						<td>
							<h5><i class="far fa-calendar-alt mr-2"></i> Aktif Sejak</h5>
							<div class="d-block d-lg-none">
								<h5><span class="mr-2"></span><b><?=$merchant_subscription['subscription_start'] ? date('d / m / Y', strtotime($merchant_subscription['subscription_start'])) : '-'?></b></h5>
								<hr>
							</div>
						</td>
						<td class="d-none d-lg-block"><h5>: <b><?=$merchant_subscription['subscription_start'] ? date('d / m / Y', strtotime($merchant_subscription['subscription_start'])) : '-'?></b></h5></td>
					</tr>
					<tr>
						<td>
							<h5><i class="far fa-calendar-alt mr-2"></i> Berakhir</h5>
							<div class="d-block d-lg-none">
								<h5><span class="mr-2"></span><b><?=$merchant_subscription['subscription_expired'] ? date('d / m / Y', strtotime($merchant_subscription['subscription_expired'])) : '-'?></b></h5>
								<hr>
							</div>
						</td>
						<td class="d-none d-lg-block"><h5>: <b><?=$merchant_subscription['subscription_expired'] ? date('d / m / Y', strtotime($merchant_subscription['subscription_expired'])) : '-'?></b></h5></td>
					</tr>
					<tr>
						<td>
							<h5><i class="fas fa-user mr-2"></i>Max. Antrian</h5>
							<div class="d-block d-lg-none">
								<h5><span class="mr-2"></span><b><?=$merchant_subscription['max'] ? number_format($merchant_subscription['max'],false,',','.').' / Hari' : SUBSCRIPTION_FREE_MAX_QUEUE.' / Hari'?></b></h5>
								<hr>
							</div>
						</td>
						<td class="d-none d-lg-block"><h5>: <b><?=$merchant_subscription['max'] ? number_format($merchant_subscription['max'],false,',','.').' / Hari' : SUBSCRIPTION_FREE_MAX_QUEUE.' / Hari'?></b></h5></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-2 mb-5">
				<?php if (!empty($merchant_subscription)){?>
				<button class="btn btn-outline-danger w-100" id="deleteSubscription" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-times-circle"></i> Hapus Paket</button>
				<div class="dropdown-menu dropdown-menu-right text-center" aria-labelledby="deleteSubscription">
					<p class="text-secondary">Apakah anda yakin? Tindakan ini tidak dapat dibatalkan</p>
					<button id="confirm_delete" class="btn btn-danger">Konfirmasi Hapus</button>
				</div>
				<?php }?>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-7">
				<!-- Alert Subscription Expired -->
				<?php if (date("y-m-d", strtotime('-3 days', strtotime($merchant_subscription['subscription_expired']))) <= date("y-m-d")){?>
					<div class="col-12 mb-3 py-2 px-4 text-danger">
						<i class="fas fa-exclamation-circle mr-2"></i>
						Paket layanan anda akan segera berakhir. Jika tidak diperpanjang, maka anda akan menggunakan tipe Paket Gratis. Silahkan perpanjang layanan atau membeli paket layanan yang lain.
					</div>
				<?php }?>
				<!-- End of Alert Subscription Expired -->
				<!-- Alert Pending Subscription -->
				<?php if(!empty($pending_subscription)){?>
					<div class="col-12 bg-dark-orange text-light rounded mb-5 py-2 px-4">
						<?php $invoice_end = local("d/m/Y H:i", strtotime($pending_subscription['created_date']), "+6 hours");?>
						<i class="fas fa-exclamation-circle mr-2"></i>
						Silahkan selesaikan tagihan pembayaran paket anda. Tagihan akan terhapus otomatis pada <?=$invoice_end?> . Atau anda bisa memilih paket yang lain di bawah ini (tagihan sebelumnya akan terhapus).
					</div>
				<?php }?>
				<!-- End of Pending Subscription -->
				<!-- Button Toogle Show Subscription -->
				<?php if (!empty($merchant_subscription) || !empty($pending_subscription)){?>
					<button id="toogle_subscription" class="btn btn-outline-primary col-12 col-lg-auto"><i id="toogle_subscription_indicator" class="fas fa-chevron-circle-down mr-2"></i><span id="toogle_subscription_text">Tampilkan pilihan paket</span></button>
				<?php }?>
				<!-- End of Button Toogle Show Subscription -->
			</div>
		</div>
		</div>
	</div>
	<div class="space" style="height: 30px"></div>
	<div id="subscription_selection" style="display: <?=!empty($merchant_subscription) || !empty($pending_subscription) ? 'none' : ''?>">
		<div class="col-10 mx-auto mb-3">
			<span class="text-tosca"><h1><i class="fas fa-shopping-cart mr-2"></i>Pilih Paket</h1></span>
			<hr>
			<?php if (!empty($merchant_subscription) || !empty($pending_subscription)){?>
				<span>
					<i class="fas fa-exclamation-circle text-danger mr-2"></i>
					<?php if (!empty($merchant_subscription)){?>
						Memperpanjang langganan akan menambah masa aktif paket sesuai durasi. 
					<?php }?>
					Membeli paket lain, akan menghapus paket sebelumnya dan menghapus tagihan pembayaran sebelumnya (jika ada).
				</span>
			<?php }?>
		</div>
		<div class="row">
			<div class="col-10 mx-auto bg-light box-subscription">
				<div class="row justify-content-center">
					<?php foreach ($subscriptions as $subscription) {?>
					<?php 
					$img_url = base_url(IMG_FOLDER.'subscriptions/'.$subscription['logo']);
					$btn_text_buy = isset($merchant_subscription['id_subscription_type']) && $merchant_subscription['id_subscription_type'] == $subscription['id_subscription_type'] ? 'Perpanjang Langganan' : 'Beli Paket';
					?>
					<div class="col-12 col-lg-3 mx-2 my-3 border border-secondary rounded shadow-soft">
						<!-- Header Title -->
						<div class="row bg-primary" style="height: 300px;background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('<?=$img_url?>');background-size: cover;">
							<div class="col-12 px-2 py-4 text-center my-auto">
								<span class="h1 text-light"><?= $subscription['name']?></span><br><br>
								<span class="text-light"><i>Durasi Langganan : </i><b><?= $subscription['duration']?> Hari</b></span><br>
								<span class="text-light"><i>Maksimal Antrian : </i><b><?= number_format($subscription['max'],false,',','.')?> / Hari</b></span><br>
							</div>
						</div>
						<!-- End of Header Title -->
						<!-- Description -->
						<div class="row">
							<div class="col-12 p-3 text-center">
								<div class="row" style="height: 80px">
									<div class="m-auto col-12">
										<!-- Price Section -->
										<?php if ($subscription['discount']){?>
										<label class="h5 text-muted"><del>Rp.<?= number_format($subscription['price'],0,',','.')?></del></label><br>
										<label class="h2 text-success"><b>Rp.<?= number_format($subscription['price_after_discount'],0,',','.')?></b></label>
										<?php } else {?>
										<label class="h2 text-success"><b>Rp.<?= number_format($subscription['price'],0,',','.')?></b></label>
										<?php }?>
										<!-- End of Price Section -->
										<!-- Buy Button -->
									</div>
								</div>
								<hr>
								<a href="<?= base_url('merchant/subscription/buy/'.$subscription['id_subscription_type'])?>">
									<button class="btn col-12 btn-outline-success"><?=$btn_text_buy?></button>
								</a>
								<!-- End of Buy Button -->
							</div>
						</div>
						<!-- End of Description -->
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
	<div class="space" style="height: 80px"></div>
	<div class="col-10 mx-auto">
		<div class="row d-flex justify-content-between">
			<div class="col-12 col-lg-7">
				<!-- content description left -->
				<h2 class="text-dark">Memilih paket yang tepat untuk usaha anda</h2><br>
				<p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula.</p>
				<p class="text-secondary">Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis.</p>
				<p class="text-secondary">Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis.</p>
				<p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula.</p>
			</div>
			<div class="col-12 col-lg-4">
				<img class="img-fluid" src="<?= base_url(IMG_HTML_FOLDER.'label-benefit.png')?>"><br><br>
				<!-- Benefit Statistic -->
				<div class="col-12 border border-primary rounded shadow-tiny">
					<div class="row">
						<div class="col-2 text-center p-2">
							<div class="row h-100">
								<i class="h4 fa fa-chart-line text-tosca m-auto"></i>
							</div>
						</div>
						<div class="col-10 text-center bg-tosca p-2">
							<div class="row h-100">
								<label class="h5 text-white m-auto">Laporan Statistik Antrian</label>
							</div>
						</div>
					</div>
				</div>

				<div class="space" style="height: 30px"></div>
				<!-- Benefit CS -->
				<div class="col-12 border border-primary rounded shadow-tiny">
					<div class="row">
						<div class="col-2 text-center p-2">
							<div class="row h-100">
								<i class="h4 fa fa-phone text-tosca m-auto"></i>
							</div>
						</div>
						<div class="col-10 text-center bg-tosca p-2">
							<div class="row h-100">
								<label class="h5 text-white m-auto">Customer Support</label>
							</div>
						</div>
					</div>
				</div>

				<div class="space" style="height: 30px"></div>
				<!-- Benefit Control Access -->
				<div class="col-12 border border-primary rounded shadow-tiny">
					<div class="row">
						<div class="col-2 text-center p-2">
							<div class="row h-100">
								<i class="h4 fas fa-desktop text-tosca m-auto"></i>
							</div>
						</div>
						<div class="col-10 text-center bg-tosca p-2">
							<div class="row h-100">
								<label class="h5 text-white m-auto">Full Control Access</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="space" style="height: 60px"></div>
</div>