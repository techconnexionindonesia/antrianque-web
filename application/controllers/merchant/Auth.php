<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('merchant_lib');
		$this->load->library('authenticator');
		$this->load->library('verification_lib');	
		$this->load->helper('utility_helper');
	}

	public function index() {
		redirect('auth/login');
	}

	public function login() {
		$this->page_criteria('not_loged');
		$this->data['title'] = "AntrianQue - Login Merchant";
		$this->data['meta_description'] = "Login Merchant AntrianQue dan atur antrian anda sekarang juga !";
		if ($this->session->flashdata('login_status')){
			$this->data['status'] = $this->session->flashdata('login_status');
		}
		if ($this->input->post())
			$this->do_login();
		else
			$this->page_display->display_merchant('auth/login',$this->data,true);
	}

	public function signup() {
		$this->page_criteria('not_loged');
		$this->data['title'] = "AntrianQue - Daftar Merchant";
		$this->data['meta_description'] = "Daftar sebagai Merchant. Gunakan teknologi terkini untuk antrian pada bisnis anda dan mudahkan segalanya !";
		$this->data['custom_js'] = array('merchant/auth/signup');
		if ($this->input->post())
			$this->do_signup();
		else
			$this->page_display->display_merchant('auth/signup',$this->data,true);
	}

	public function pending() {
		$this->page_criteria('pending');
		$this->data['title'] = "AntrianQue - Pending Verification";
		$this->data['meta_noindex'] = true;
		if ($this->session->flashdata('resend_verification_status')){
			$this->data['resend'] = true;
		}
		$this->page_display->display_merchant('auth/pending',$this->data,true);
	}

	public function check_email() {
		$result['result'] = false;
		if ($this->input->post('email')){
			$email = $this->input->post('email');
			$result['result'] = $this->merchant_lib->do_check_email($email);
		}
		echo json_encode($result);
	}

	public function logout() {
		$this->authenticator->logout_merchant();
		redirect('merchant/auth/login');
	}

	public function resend_verification() {
		 if ($this->is_loged_merchant()){
			$merchant_status = $this->get_merchant_session_status();
			if ($merchant_status == 'pending'){
				$token_verification = $this->verification_lib->create_initial_verification($this->get_merchant_session_id());
				$this->session->set_flashdata('resend_verification_status', true);
				$url_verification = base_url('verification/verify/'.$token_verification);
				$merchant_data = $this->merchant_lib->get_merchant_data($this->get_merchant_session_id());
                $data = array(
                    'email' => $merchant_data['email'],
                    'name' => $merchant_data['name'],
                    'url' => $url_verification
                );
                $this->email_lib->do_send_verification($data);
			}
			redirect('merchant/auth/pending');
		} else {
			redirect('merchant/auth/login');
		}
	}

	private function do_login(){
		$data = $this->input->post();
		if (!$this->is_loged_merchant()){
			$result_authentication = $this->authenticator->auth_merchant($data);
			if ($result_authentication){
				$this->redirect_by_status($result_authentication);
			} else {
				$this->session->set_flashdata('login_status', 'failed');
				redirect('merchant/auth/login');
			}
		} else {
			redirect('dashboard');
		}
	}

	private function do_signup(){
		$data = $this->input->post();
		$data = trim_by_array($data);
		if (isset($data['email'])){
			$data['email'] = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
		}
		if (!$this->is_loged_merchant()){
			$this->merchant_lib->register($data);
			$this->merchant_activity_logger->log('Register');
			$this->redirect_by_status('pending');
		}
	}

	private function redirect_by_status($status){
		switch ($status) {
			case 'pending':
				$this->merchant_activity_logger->log('Login');
				$this->merchant_lib->update_last_login();
				redirect('merchant/auth/pending');
				break;
			
			case 'active':
				$this->merchant_activity_logger->log('Login');
				$this->merchant_lib->update_last_login();
				redirect('merchant/dashboard');
				break;

			case 'suspended':
				redirect('merchant/auth/login');
				break;

			case 'deleted':
				redirect('merchant/auth/login');
				break;
		}
	}

}
