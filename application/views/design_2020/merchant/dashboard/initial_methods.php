<div class="container-fluid">
	<div class="row vh-100">
		<div class="col-12 bg-tosca">
			<div class="row h-100">
				<div class="col-10 col-lg-6 m-auto p-4 bg-light border rounded text-center">
					<span class="h2">Selamat Datang di AntrianQue</span>
					<hr><br>
					<div class="row">
						<div class="col-12 col-lg-6 p-4">
							<h2 class="text-secondary">PIN</h2>
							<div class="row" style="height: 400px">
								<div class="border rounded m-auto p-2">
									<span class="display-4"><?= isset($methods['pin'])? $methods['pin'] : ''?></span>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-6 p-4">
							<h2 class="text-secondary">QR Code</h2>
							<div class="row" style="height: 400px">
								<div class="border rounded m-auto p-2 col-10">
									<img class='img-fluid' src="<?= isset($methods['qr_url']) ? base_url().IMG_FOLDER.'qr/'.$methods['qr_url'] : ''?>">
								</div>
							</div>
						</div>
					</div>
					<p class="text-muted">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula. Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem.
					</p>
					<div class="row">
						<div class="col-12 col-lg-6 p-4">
							<a href="<?= base_url().'merchant/option/print'?>">
								<button class="btn btn-outline-dark col-12">Cetak</button>
							</a>
						</div>
						<div class="col-12 col-lg-6 p-4">
							<a href="<?= base_url().'merchant/dashboard/initial/subscription'?>">
							<button class="btn btn-primary col-12">Selanjutnya</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>