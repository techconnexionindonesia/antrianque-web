<div class="container-fluid">
	<div class="row vh-100">
		<div class="col-12 bg-tosca">
			<div class="row h-100">
				<div class="col-10 col-lg-6 m-auto p-4 bg-light border rounded text-center">
					<span class="h2">Verifikasi Akun Anda</span>
					<hr><br>
					<i class="far fa-paper-plane icon-email-sent"></i><br><br>
					<p class="text-muted">Anda telah berhasil melakukan registrasi. Silahkan cek email anda untuk melakukan verifikasi akun Merchant AntrianQue anda. Jika anda belum dapat menerima email verifikasi, silahkan klik tombol di bawah untuk mengirim email verifikasi kembali.</p>
					<a href="<?= base_url('merchant/auth/resend_verification')?>"><button class="btn btn-outline-primary col-8">Kirim Ulang Verifikasi</button></a><br><br>
					<a href="<?= base_url('merchant/auth/logout')?>"><button class="btn btn-primary col-8">Logout</button></a><br><br>
					<?php if(isset($resend)){?>
					<span class="text-success"><i class="far fa-check-circle mr-2"></i>Email verifikasi berhasil dikirim ulang</span>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>