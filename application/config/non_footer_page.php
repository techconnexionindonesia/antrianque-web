<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['non_footer_page_merchant'] = array(
	'auth/login',
	'auth/signup',
	'dashboard/initial'
);