/*
 * Javascript
 * AntrianQue Web
 * -
 * Signup Function
*/

$(document).ready(function(){

	$('#signupForm1Submit').click(function(){
		$('#signupForm1Loading').show();
		setTimeout(function(){
			do_check_form_1();
		},100);
	});

	$('#signupForm2Back').click(function(){
		transition('#signupForm2','#signupForm1');
	});

	$('input').keyup (function(){
		reset_error();
	});

	function do_check_form_1() {
		var is_email_exist = $('#email').val();
		var is_password_exist = $('#password').val() && $('#password').val().length >= 6;

		if (!is_email_exist){
			$('#signupFormEmailEmpty').show(500);
		}

		if (!is_password_exist){
			$('#signupFormPasswordEmpty').show(500);
		}

		var result_email = check_email_availability();
		var result_password = /\s/.test($('#password').val()) == false;
		var result_repassword = $('#password').val() == $('#repassword').val();

		if (!result_email) {
			$('#signupFormEmailError').show(500);
		}

		if (!result_password) {
			$('#signupFormPasswordError').show(500);
		}

		if (!result_repassword) {
			$('#signupFormRepasswordError').show(500);
		}

		if (result_email && result_password && result_repassword && is_email_exist && is_password_exist){
			transition('#signupForm1','#signupForm2');
		}

		$('#signupForm1Loading').hide();
	}

	function check_email_availability() {
		var email = $('#email').val();
		var data = {
			email : email
		}
		var result = do_ajax_post_sync(URL_AJAX_CHECK_EMAIL,data,'POST','json');
		var response = get_response_ajax(result, 'json');
		if (response.result == true) {
			return false;
		} else {
			return true;
		}
	}

	function reset_error() {
		$('#signupFormEmailEmpty').hide();
		$('#signupFormPasswordEmpty').hide();
		$('#signupFormEmailError').hide();
		$('#signupFormPasswordError').hide();
		$('#signupFormRepasswordError').hide();
	}

	function transition(from,to) {
		$(from).hide(500);
		$(to).show(500);
	}

});