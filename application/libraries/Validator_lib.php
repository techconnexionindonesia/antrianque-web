<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class Validator_lib {
 
	public function validate($data,$type){
        $result = true;
        if ($type == "queue_available"){
            if (is_string($data) == false || in_array($data, array(0,1)) == false){
                $result = false;
            }
        }

        return $result;
    }
 
}
 