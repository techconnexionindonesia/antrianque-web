<?php

class Verification_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= VERIFICATION_TABLE;
    protected $_primary_key = VERIFICATION_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}