<!-- Navbar Section -->
<nav class="navbar navbar-expand-lg navbar-dark bg-tosca navbar-main">
  <a class="navbar-brand" href="<?= base_url('merchant/')?>">
    <img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-white-transparent.png" width="90" height="90" class="d-inline-block align-middle mr-2" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="row col-11 d-none d-lg-flex">
    <div class="col-1 space"></div>
    <div class="col-7">
      <ul class="navbar-nav">
        <li class="nav-item mr-5">
          <a href="<?= base_url('merchant/')?>">
            <span class="h5 text-light"><i class="fa fa-home mr-2" aria-hidden="true"></i>Dashboard</span>
          </a>
        </li>
        <li class="nav-item mr-5">
          <a href="<?= base_url('merchant/option')?>">
            <span class="h5 text-light"><i class="fa fa-cog mr-2" aria-hidden="true"></i>Pengaturan</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url('merchant/statistic')?>">
            <span class="h5 text-light"><i class="fa fa-chart-bar mr-2"></i>Statistik</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="col-4 pr-4 d-flex flex-row-reverse">
      <div class="col-1 space"></div>
      <div class="dropdown">
        <span class="text-light dropdown-toggle cursor-pointer h5 cursor-pointer" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?= isset($merchant_data['name']) ? $merchant_data['name'] : ''?>
        </span>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="<?= base_url('merchant/profile')?>"><i class="fa fa-user mr-2" aria-hidden="true"></i>Profil</a>
          <a class="dropdown-item" href="<?= base_url('merchant/subscription')?>"><i class="fa fa-credit-card mr-2" aria-hidden="true"></i>Paket Langganan</a>
          <a class="dropdown-item" href="<?= base_url('merchant/auth/logout')?>"><i class="fas fa-sign-in-alt mr-2"></i>Logout</a>
        </div>
      </div>
    </div>
  </div>
  <div class="collapse navbar-collapse" id="navbarMain">
    <div id="navbar-link-mobile">
      <ul class="navbar-nav mt-3">
        <li class="nav-item">
          <a href="<?= base_url('merchant/')?>" class="text-light"><i class="fa fa-home mr-2" aria-hidden="true"></i>Dashboard</a>
        </li>
        <li class="nav-item">
          <hr style="border-color: rgb(200,200,200);">
          <a href="<?= base_url('merchant/setting')?>" class="text-light"><i class="fa fa-cog mr-2" aria-hidden="true"></i>Pengaturan</a>
        </li>
        <li class="nav-item">
          <hr style="border-color: rgb(200,200,200);">
          <a href="<?= base_url('merchant/statistic')?>" class="text-light"><i class="fa fa-chart-bar mr-2"></i>Statistik</a>
        </li>
        <li class="nav-item">
          <hr style="border-color: rgb(200,200,200);">
          <a href="<?= base_url('merchant/profile')?>" class="text-light"><i class="fa fa-user mr-2" aria-hidden="true"></i>Profil</a>
        </li>
        <li class="nav-item">
          <hr style="border-color: rgb(200,200,200);">
          <a href="<?= base_url('merchant/subscription')?>" class="text-light"><i class="fa fa-credit-card mr-2" aria-hidden="true"></i>Paket Langganan</a>
        </li>
        <li class="nav-item">
          <hr style="border-color: rgb(200,200,200);">
          <a href="<?= base_url('merchant/auth/logout')?>" class="text-light"><i class="fas fa-sign-in-alt mr-2"></i>Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- End of Navbar -->