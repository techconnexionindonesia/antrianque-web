<div class="container">
	<div class="space" style="height: 80px"></div>
	<div class="row justify-content-between">
		<div class="col-lg-7">
			<h3 class="text-secondary font-weight-bold">Konfirmasi Pembelian</h3>
			<hr>
			<div class="space" style="height: 30px"></div>
			<!-- Subscription Name -->
			<div class="col-11">
				<!-- Name -->
				<div class="row h-100">
					<div class="bg-primary text-light p-2 h5 mr-3"><i class="fas fa-shopping-cart"></i></div>
					<h4 class="my-auto text-secondary">Nama Paket</h5>
				</div>
				<div class="row">
					<h3 class="text-primary"><?= isset($subscription) ? $subscription['name'] : ''?></h3>
				</div>
				<div class="space" style="height: 30px"></div>
				<!-- Max -->
				<div class="row h-100">
					<div class="bg-primary text-light p-2 h5 mr-3"><i class="fas fa-user"></i></div>
					<h4 class="my-auto text-secondary">Max Antrian</h5>
				</div>
				<div class="row">
					<h4><?= isset($subscription) ? number_format($subscription['max'],false,',','.') : ''?> antrian / hari</h4>
				</div>
				<div class="space" style="height: 30px"></div>
				<!-- Duration -->
				<div class="row h-100">
					<div class="bg-primary text-light p-2 h5 mr-3"><i class="fas fa-clock"></i></div>
					<h4 class="my-auto text-secondary">Durasi</h5>
				</div>
				<div class="row">
					<h4><?= isset($subscription) ? number_format($subscription['duration'],false,',','.') : ''?> hari</h4>
				</div>
				<div class="space" style="height: 30px"></div>
				<!-- Expiry -->
				<div class="row h-100">
					<div class="bg-primary text-light p-2 h5 mr-3"><i class="fas fa-shopping-cart"></i></div>
					<h4 class="my-auto text-secondary"><?=$label?></h5>
				</div>
				<div class="row">
					<?php $end = isset($subscription_expired) ? $subscription_expired : '' ;?>
					<h4><?= $end?></h4>
				</div>
				<div class="space" style="height: 30px"></div>
			</div>
		</div>
		<div class="col-lg-4 bg-light border rounded p-3">
			<div class="space" style="height: 50px"></div>
			<!-- Price -->
			<div class="row">
				<div class="col-6">
					<h5 class="text-secondary"><b><?= $subscription['name']?></b></h5>
				</div>
				<div class="col-6 text-success text-right">
					<h4><b>Rp.<?=isset($subscription['price']) ? number_format($subscription['price'],false,',','.') : ''?></b></h4>
				</div>
			</div>
			<div class="space" style="height: 10px"></div>
			<!-- Discount -->
			<div class="row">
				<div class="col-6">
					<span class="text-secondary">Diskon</span>
				</div>
				<div class="col-6 text-success text-right">
					<h5><?=isset($subscription['discount']) ? $subscription['discount'] : 0 ?>%</h5>
				</div>
			</div>
			<div class="space" style="height: 10px"></div>
			<!-- Payment Gateway Fee -->
			<div class="row">
				<div class="col-6">
					<span class="text-secondary">Biaya Administrasi</span>
				</div>
				<div class="col-6 text-success text-right">
					<h5>Rp.<?= $fee ?  number_format($fee,false,',','.'): 0 ?></h5>
				</div>
			</div>
			<div class="space" style="height: 10px"></div>
			<hr>
			<div class="space" style="height: 10px"></div>
			<!-- Total -->
			<div class="row">
				<div class="col-6">
					<span class="text-secondary">Total</span>
				</div>
				<div class="col-6 text-success text-right">
					<h4><b>Rp.<?= isset($subscription['total']) ?  number_format($subscription['total'],false,',','.'): 0 ?></b></h4>
				</div>
			</div>
			<div class="space" style="height: 10px"></div>
			<input type="hidden" id="id_subscription" value="<?= isset($subscription['id_subscription_type']) ? $subscription['id_subscription_type'] : ''?>">
			<label><input type="checkbox" id="agreement"> <span class="text-secondary">
			Saya setuju dengan <a href="<?=base_url('terms_and_conditions')?>">Syarat dan Ketentuan</a>
			</span></label>
			<div class="space" style="height: 10px"></div>
			<button id="buySubmit" class="btn btn-success col-12" disabled><i class="fas fa-shopping-cart mr-2"></i>Lanjut ke Pembayaran</button>
			<div class="space" style="height: 10px"></div>
			<span class="text-secondary"><small>* Pembayaran dapat dilakukan melalui Transfer Bank, VA Bank, Mini Market, dan Dompet Digital dalam 6 jam setelah memilih metode pembayaran</small></span>
		</div>
	</div>
	<div class="space" style="height: 30px"></div>
	<div class="row justify-content-center">
		<div class="col-lg-3 text-secondary text-center mx-3 mb-3">
			<div class="row" style="height: 180px">
				<img class="img-fluid m-auto" src="<?=base_url().IMG_HTML_FOLDER.'payment-procedure-1.png'?>"><br><br>
			</div>
			Cek kembali detail pada paket yang akan dibayar. Klik "Lanjut ke Pembayaran" untuk melakukan pembayaran
		</div>
		<div class="col-lg-1 d-none d-lg-block text-center">
			<div class="row" style="height: 180px">
				<img class="img-fluid m-auto" src="<?=base_url().IMG_HTML_FOLDER.'divider-2.png'?>">
			</div>
		</div>
		<div class="col-lg-3 text-secondary text-center mx-3 mb-3">
			<div class="row" style="height: 180px">
				<img class="img-fluid m-auto" src="<?=base_url().IMG_HTML_FOLDER.'payment-procedure-2.png'?>"><br><br>
			</div>
			Pilih metode pembayaran yang tersedia, lalu lakukan pembayaran sesuai instruksi
		</div>
		<div class="col-lg-1 d-none d-lg-block text-center">
			<div class="row" style="height: 180px">
				<img class="img-fluid m-auto" src="<?=base_url().IMG_HTML_FOLDER.'divider-2.png'?>">
			</div>
		</div>
		<div class="col-lg-3 text-secondary text-center">
			<div class="row" style="height: 180px">
				<img class="img-fluid m-auto" src="<?=base_url().IMG_HTML_FOLDER.'payment-procedure-3.png'?>"><br><br>
			</div>
			Setelah melakukan pembayaran, sistem secara otomatis menerima pembayaran anda dan mengaktifkan paket anda
		</div>
	</div>
	<div class="space" style="height: 60px"></div>
</div>