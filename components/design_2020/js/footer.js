$(document).ready(function(){
	if ($('.footer-status').length){
		$('.footer-status').click(function(){
			if ($('.footer-status').data('value') == 'off'){
				switch_status_on();
			} else {
				switch_status_off();
			}
		})
	}

	if ($('.footer-reset').length){
		$('.footer-reset').mousedown(function(){
			$('#footer_reset_audio')[0].play();
		});
	}

	if ($('.footer-reset-item').length){
		$('.footer-reset-item').click(function(){
			reset_queue_numbering(this);
		});
	}

	if ($('#cs_submit').length){
		$('#cs_submit').click(function(){
			cs_send_message();
		});
	};

	function switch_status_on() {
		$('.footer-status').removeClass('footer-status-off');
		$('.footer-status').addClass('footer-status-on');
		$('#footer_status_audio')[0].play();
		set_footer_progress('status','loading');
		do_ajax_post(URL_AJAX_MERCHANT_DASHBOARD_SET_STATUS,{status : 1},'POST','json',function(err,result){
			if (!err && result.status == 'success'){
				$('.footer-status').data('value','on');
				set_footer_progress('status','success',true);
			} else {
				$('.footer-status').removeClass('footer-status-on');
				$('.footer-status').addClass('footer-status-off');
				$('#footer_status_audio')[0].play();
				set_footer_progress('status','failed',true);
				show_alert_failed();
			}
		});
	}

	function switch_status_off() {
		$('.footer-status').removeClass('footer-status-on');
		$('.footer-status').addClass('footer-status-off');
		$('#footer_status_audio')[0].play();
		set_footer_progress('status','loading');
		do_ajax_post(URL_AJAX_MERCHANT_DASHBOARD_SET_STATUS,{status : 0},'POST','json',function(err,result){
			if (!err && result.status == 'success'){
				$('.footer-status').data('value','off');
				set_footer_progress('status','success',true);
			} else {
				$('.footer-status').removeClass('footer-status-off');
				$('.footer-status').addClass('footer-status-on');
				$('#footer_status_audio')[0].play();
				set_footer_progress('status','failed',true);
				show_alert_failed();
			}
		});
	}

	function reset_queue_numbering(reset) {
		var target_prefix = $(reset).data('target');
		set_footer_progress('reset','loading');
		do_ajax_post(URL_AJAX_MERCHANT_DASHBOARD_RESET_NUMBERING,{prefix : target_prefix},'POST','json',function(err,result){
			if (!err && result.status == 'success'){
				set_footer_progress('reset','success',true);
			} else {
				set_footer_progress('reset','failed',true);
				show_alert_failed();
			}
		});
	}

	function set_footer_progress(dom,progress,hide_after_show) {
		$('#footer_'+dom+'_loading').hide();
		$('#footer_'+dom+'_success').hide();
		$('#footer_'+dom+'_failed').hide();
		$('#footer_'+dom+'_'+progress).show();
		if (hide_after_show){
			setTimeout(function(){
				$('#footer_'+dom+'_loading').hide();
				$('#footer_'+dom+'_success').hide();
				$('#footer_'+dom+'_failed').hide();
			},3000);
		}
	}

	function cs_send_message() {
		var cs_report_message = $('#cs_report_message').val();
		if (cs_report_message){
			show_loading();
			disable('#cs_submit');
			do_ajax_post(URL_AJAX_MERCHANT_DASHBOARD_CS_SUBMIT,{message : cs_report_message},'POST','json',function(err,result){
				hide_loading();
				if (result && result.status == 'success'){
					show_alert_success("Pesan berhasil dikirim");
					$('#csModal').modal('toggle');
					$('#cs_report_message').val("");
				} else {
					show_alert_failed("Pesan tidak berhasil dikirim");
				}
				enable('#cs_submit');
			});
		} else {
			show_alert_failed("Isi pesan tidak boleh kosong");
		}
	}
});