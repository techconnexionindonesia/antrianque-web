$(document).ready(function(){
  startTime();

  function startTime() {
    var today = new Date();
    var hour = today.getHours();
    var minute = today.getMinutes();
    var second = today.getSeconds();
    minute = checkTime(minute);
    second = checkTime(second);
    var d = new Date(),
    months = ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'],
    days = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
    d = days[d.getDay()]+' , '+d.getDate()+' '+months[d.getMonth()]+' '+d.getFullYear();
    $('.clock-display').html(d + "<br>" + hour + ":" + minute + ":" + second);
    var t = setTimeout(startTime, 1000);
  }

  function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
  }
});