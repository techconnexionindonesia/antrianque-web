<?php

class Support_request_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= SUPPORT_REQUEST_TABLE;
    protected $_primary_key = SUPPORT_REQUEST_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}