/* Variable constant */
var default_alert_show_second = 3;

var URL_AJAX_CHECK_EMAIL = base_url + "merchant/auth/check_email";
var URL_AJAX_MERCHANT_PROFILE_UPDATE = base_url + "merchant/profile/do_update";
var URL_AJAX_MERCHANT_SUBSCRIPTION_DELETE = base_url + "merchant/subscription/delete";
var URL_AJAX_MERCHANT_SUBSCRIPTION_BUY = base_url + "merchant/subscription/do_payment";
var URL_AJAX_MERCHANT_DASHBOARD_SET_STATUS = base_url + "merchant/dashboard/set_status";
var URL_AJAX_MERCHANT_DASHBOARD_RESET_NUMBERING = base_url + "merchant/dashboard/reset_numbering";
var URL_AJAX_MERCHANT_DASHBOARD_CS_SUBMIT = base_url + "merchant/dashboard/cs_submit_message";