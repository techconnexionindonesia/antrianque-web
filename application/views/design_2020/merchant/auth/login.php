<div class="container-fluid">
	<div class="row vh-100">
		<!-- Left / Top Section -->
		<div class="col-12 col-lg-4">
			<div class="row h-100">
				<div class="col-12 my-auto p-5">
					<div class="text-center">
						<div class="mt-5 text-center">
							<img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-nt-blue.png" class="mr-2" width="50" height="50">
							<span class="h2 align-middle text-tosca"><b>AntrianQue</b></span>
						</div>
						<hr>
					</div>
					<span class="h4 text-tosca"><strong><i class="fas fa-sign-in-alt mr-2 mb-3"></i>LOGIN</strong></span><br>
					<?php echo form_open('merchant/auth/login')?>
					<div class="form-group">
						<!-- Email Section -->
						<span class="text-muted">Email :</span>
						<div class="input-group">
							<div class="input-group-prepend">
								 <span class="input-group-text"><i class="fas fa-envelope"></i></span>
							</div>
							<input type="email" name="email" class="form-control" placeholder="Email Merchant Anda" required>
						</div>
						<!-- End of Email Section -->
						<br>
						<!-- Password Section -->
						<span class="text-muted">Password :</span>
						<div class="input-group">
							<div class="input-group-prepend">
								 <span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input type="password" name="password" class="form-control" placeholder="Password Merchant Anda" required>
						</div>
						<!-- End of Password Section -->
						<br>
						<button type="submit" class="btn btn-primary w-100 mb-3"><i class="fas fa-sign-in-alt mr-2"></i>Login</button>
						<br>
						<a href="<?= base_url('merchant/auth/signup')?>">
						<button type="button" class="btn btn-outline-primary w-100"><i class="fas fa-user mr-2"></i>Daftar</button>
						</a>
					</div>
					<!-- Failed login section -->
					<?php if (isset($status)){?>
					<div class="col-12 mb-3 text-center text-danger">
						<i class="fas fa-exclamation-circle mr-2"></i>
						<?php if (isset($status) && $status == 'failed') {?>
							Anda gagal login
						<?php }?>
						<?php if (isset($status) && $status == 'suspended') {?>
							Akun anda sedang disuspend. Untuk bantuan hubungi <?= EMAIL_CS ?>
						<?php }?>
						<?php if (isset($status) && $status == 'deleted') {?>
							Akun anda telah dihapus. Untuk bantuan hubungi <?= EMAIL_CS ?>
						<?php }?>
					</div>
					<?php }?>
					<!-- End of Failed login section -->
				</div>
			</div>
		</div>
		<!-- End of Left / Top Section -->
		<!-- Right Section -->
		<div class="col-12 col-lg-8 bg-tosca" id="auth-decoration">
			<div class="row h-100">
				<div class="col-12 my-auto p-5 text-light">
					<div class="d-none d-md-block">
						<span class="display-4 font-weight-bold">Lorem Ipsum Dolor Sit</span><br><br>
						<span class="h2 font-weight-bold">simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
					</div>
					<div class="d-md-none text-center">
						<span class="h2">Lorem Ipsum Dolor Sit</span><br><br>
						<span class="h5">simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
					</div>
				</div>
			</div>
		</div>
		<!-- End of Right Section -->
	</div>
</div>