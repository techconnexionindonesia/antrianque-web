<?php

class Queue_numbering_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= QUEUE_NUMBERING_TABLE;
    protected $_primary_key = QUEUE_NUMBERING_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}