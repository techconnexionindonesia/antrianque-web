<?php

class Merchant_pending_subscription_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= MERCHANT_PENDING_SUBSCRIPTION_TABLE;
    protected $_primary_key = MERCHANT_PENDING_SUBSCRIPTION_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    public function get_merchant_pending_subscription($id_merchant) {
    	$this->db->select('*');
    	$this->db->where("
    		created_date + INTERVAL 6 HOUR > NOW() 
    		AND id_merchant = ".$id_merchant);
    	$result = parent::get();
    	return $result;
    }

}