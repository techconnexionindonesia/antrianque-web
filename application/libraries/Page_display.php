<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Page_display {

	public function __construct(){
		$this->CI =& get_instance();
		$this->template = $this->CI->config->item('template');
		$this->CI->load->model('merchant_queue_option_model');
		$this->CI->load->model('queue_numbering_model');
	}

	public function display_merchant($page, $data = false, $hide_menu = false){
		$page_name = 'merchant/'.$page;
		$this->display($page_name, $data, false, $hide_menu);
	}

	public function display($page, $data = false, $is_admin = false, $hide_menu = false){
		$component_folder = base_url().'components/'.$this->template.'/';
		$data['component_folder'] = $component_folder;
		self::header($data);
		self::menu($data, $is_admin, $hide_menu);
		$this->CI->load->view($this->template.'/'.$page, $data);
		self::message_modal();
		self::footer($data);
	}

	private function menu($data, $is_admin = false, $hide_menu = false){
		if (!$hide_menu){
			if ($is_admin){
				$this->CI->load->view($this->template.'/include/menu', $data);
			} else {
				$this->CI->load->view($this->template.'/include/menu', $data);
			}
		}
	}

	private function header($data){
		$this->CI->load->view($this->template.'/include/header', $data);
	}

	private function message_modal(){
		$this->CI->load->view($this->template.'/include/message_modal');
	}

	private function footer($data){
		$data['queue_available'] = $this->get_merchant_queue_status();
		$data['queue_numbering'] = $this->get_queue_numbering();
		$this->CI->load->view($this->template.'/include/footer', $data);
	}

	public function get_page_only($page, $data, $return = false){
		if($page){
			return $this->CI->load->view($this->template.'/'.$page, $data, $return);
		}
	}

	public function get_email_template($template_name, $data){
		if($template_name){
			$template = $this->CI->load->view('email_template/'.$template_name, $data, true);
			foreach ($data as $var_name => $var_value) {
				$search_key = '/'.$var_name.'/';
				$template = str_replace($search_key, $var_value, $template);
			}

			$base = $this->CI->load->view('email_template/base_email', null, true);
			$template = str_replace('/content_template/', $template, $base);
			return $template;
		}
	}

	private function get_merchant_queue_status(){
		if ($this->CI->session->userdata('id_merchant')){
			$id_merchant = $this->CI->session->userdata('id_merchant');
			$data_option = $this->CI->merchant_queue_option_model->get(array('id_merchant' => $id_merchant));
			$data_option = reset($data_option);
			return $data_option['queue_available'];
		} else {
			return false;
		}
	}

	private function get_queue_numbering(){
		if ($this->CI->session->userdata('id_merchant')){
			$id_merchant = $this->CI->session->userdata('id_merchant');
			$data_numbering = $this->CI->queue_numbering_model->get(array('id_merchant' => $id_merchant));
			return $data_numbering;
		} else {
			return false;
		}
	}
}