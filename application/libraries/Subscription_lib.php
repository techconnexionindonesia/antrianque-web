<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class Subscription_lib extends MY_Controller{
 
	public function __construct() { 
		$this->CI = &get_instance();
        $this->CI->load->model('merchant_subscription_model');
        $this->CI->load->model('merchant_pending_subscription_model');
    }

    public function disable_merchant_active_subscription($id_merchant = false, $is_cancelled_by_merchant = 0) {
    	$result = false;
    	if (!$id_merchant){
    		$id_merchant = $this->CI->get_merchant_session_id();
    	}
    	$active_subscription = $this->CI->merchant_subscription_model->get_merchant_active_subscription($id_merchant);
    	if (!empty($active_subscription['id_merchant_subscription'])){
    		$id_merchant_subscription = $active_subscription['id_merchant_subscription'];
    		$criteria_update = array(
    			'disabled' => 1,
    			'cancelled_by_merchant' => $is_cancelled_by_merchant,
    			'cancelled_date' => date('Y-m-d')
    		);
    		$result = $this->CI->merchant_subscription_model->update($criteria_update, $id_merchant_subscription);
    	}
    	return $result;
    }

    public function get_total($price,$discount = false) {
        if (!$discount)
            $discount = 0;
        $discount_calculated = $price * $discount / 100;
        $price_after_discount = $price - $discount_calculated;
        $fee = $this->get_subscription_fee($price, $discount);
        $total_price = $price_after_discount + $fee;
        return $total_price;
    }

    public function get_subscription_fee($price, $discount = false) {
        if (!$discount)
            $discount = 0;
        $discount_calculated = $price * $discount / 100;
        $price_after_discount = $price - $discount_calculated;
        $fee = $price_after_discount * PAYMENT_GATEWAY_QRIS_FEE / 100;
        if ($fee < PAYMENT_GATEWAY_GENERAL_FEE){
            $fee = PAYMENT_GATEWAY_GENERAL_FEE;
        }
        return $fee;
    }

    public function save_subscription_pending($criteria){
        $id_merchant = $criteria['id_merchant'];
        $pending_subscription = $this->CI->merchant_pending_subscription_model->get(array('id_merchant' => $id_merchant));
        $pending_subscription = reset($pending_subscription);
        if ($pending_subscription){
            $this->CI->merchant_pending_subscription_model->delete(array('id_merchant' => $id_merchant));
        }
        $this->CI->merchant_pending_subscription_model->insert($criteria);
    }

    
}
 