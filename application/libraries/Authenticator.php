<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class Authenticator {
 
	public function __construct() { 
		$this->CI = &get_instance();
        $this->CI->load->model('merchant_model');
    }

    public function auth_merchant($data) {
        if (!$this->CI->is_loged_merchant()){
            $criteria = $this->set_merchant_login_criteria($data);
            $data_merchant = $this->CI->merchant_model->get($criteria);
            $data_merchant = reset($data_merchant);
            if ($data_merchant){
                $status = $data_merchant['status'];
                if ($status == 'pending' || $status == 'active'){
                    $this->set_session_merchant($data_merchant['id_merchant'],$data_merchant['time_zone']);
                } else if ($status == 'suspended') {
                    $this->CI->session->set_flashdata('login_status','suspended');
                } else if ($status == 'deleted') {
                    $this->CI->session->set_flashdata('login_status','deleted');
                }
                return $status;
            } else {
                return false;
            }
        }
    }

    private function set_merchant_login_criteria($data) {
        $criteria = array(
            'email' => $data['email'],
            'password' => sha1($data['password'])
        );
        return $criteria;
    }

    public function set_session_merchant($id,$time_zone){
        $this->CI->session->set_userdata('id_merchant',$id);
        $this->CI->session->set_userdata('time_zone',$time_zone);
    }

    public function logout_merchant(){
        $this->CI->session->unset_userdata('id_merchant');
    }
 
}
 