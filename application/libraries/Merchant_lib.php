<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class Merchant_lib extends MY_Controller{
 
	public function __construct() { 
		$this->CI = &get_instance();
        $this->CI->load->model('merchant_model');
        $this->CI->load->model('merchant_profile_model');
        
        $this->CI->load->library('email_lib');
        $this->CI->load->library('authenticator');
        if ($this->CI->is_loged_merchant()){
            $this->id_merchant = $this->CI->session->userdata('id_merchant');
        } else {
            $this->id_merchant = false;
        }
        $this->current_time = date('Y-m-d H:i:s');
    }

    public function update_last_login() {
        if ($this->CI->is_loged_merchant()) {
            $criteria = array(
                'last_login' => date("Y-m-d H:i:s")
            );
            $this->CI->merchant_model->update($criteria, $this->id_merchant);
        }
    }

    public function do_check_email($email) {
        $result = false;
        if ($email){
            $criteria = array(
                'email' => $email
            );
            $data_merchant = $this->CI->merchant_model->get($criteria);
            $data_merchant = reset($data_merchant);
            if ($data_merchant){
                $result = true;
            }
        }
        return $result;
    }

    public function register($data) {
        $this->CI->load->library('verification_lib');
        if ($data){
            $validated = $this->validate_register_data($data);
            if ($validated) {
                // insert data merchant
                $criteria = $this->set_criteria_merchant($data);
                $id_merchant = $this->CI->merchant_model->insert($criteria);
                // insert data merchant profile
                $criteria = $this->set_criteria_merchant_profile($data, $id_merchant);
                $this->CI->merchant_profile_model->insert($criteria);
                // Create verification code
                $token_verification = $this->CI->verification_lib->create_initial_verification($id_merchant);
                $url_verification = base_url('verification/verify/'.$token_verification);
                $data = array(
                    'email' => $data['email'],
                    'name' => $data['name'],
                    'url' => $url_verification
                );
                $this->CI->email_lib->do_send_verification($data);
                $this->CI->authenticator->set_session_merchant($id_merchant);
            }
        }
    }

    public function get_merchant_data($id_merchant = false) {
        if ($id_merchant == false){
            $id_merchant = $this->CI->get_merchant_session_id();
        }
        // Get data merchant
        $data_merchant = $this->CI->merchant_model->get($id_merchant);
        $data_merchant = reset($data_merchant);
        if ($data_merchant){
            // Get data merchant profile
            $data_merchant_profile = $this->CI->merchant_profile_model->get(array('id_merchant' => $id_merchant));
            $data_merchant_profile = reset($data_merchant_profile);
            // Merge
            $data = array_merge($data_merchant, $data_merchant_profile);

            return $data;
        } else {
            return false;
        }
    }

    public function update_status($id_merchant, $status) {
        $criteria = array(
            'status' => $status
        );
        $this->CI->merchant_model->update($criteria, $id_merchant);
    }

    private function set_criteria_merchant($data) {
        $criteria = array(
            'email' => $data['email'],
            'password' => sha1($data['password']),
            'phone' => $data['phone'],
            'last_login' => $this->current_time,
            'last_reset_queue' => $this->current_time,
            'status' => MERCHANT_STATUS_PENDING
        );
        return $criteria;
    }

    private function set_criteria_merchant_profile($data, $id_merchant) {
        $criteria = array(
            'id_merchant' => $id_merchant,
            'name' => $data['name'],
            'description' => $data['description'],
            'address_line1' => $data['address_line1'],
            'address_line2' => $data['address_line2'],
            'city' => $data['city'],
            'state' => $data['state'],
            'postal_code' => $data['postal_code'],
            'country' => 'id' // hardcoded country
        );
        return $criteria;
    }

    private function validate_register_data($data) {
        $result = false;

        if (
            // Email
            (
                !empty($data['email'])
                && filter_var($data['email'], FILTER_VALIDATE_EMAIL)
                && $this->do_check_email($data['email']) == false
            )

            // Password
            &&
            (
                !empty($data['password'])
                && strlen($data['password']) >= 6
                && preg_match('/\s/', $data['password']) == false
            )

            // Merchant Name
            &&
            (
                !empty($data['name'])
            )

            // Phone
            &&
            (
                !empty($data['phone'])
                && is_numeric($data['phone'])
            )

            // Description
            &&
            (
                !empty($data['description'])
            )

            // Time Zone
            &&
            (
                !empty($data['time_zone'])
                && in_array($data['time_zone'], array('7','8','9'))
            )
        ) {
            $result = true;
        }

        return $result;
    }
 
}
 