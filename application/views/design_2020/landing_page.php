<!-- Navbar Section -->
<nav class="navbar navbar-expand-lg navbar-light navbar-landing-page">
  <a class="navbar-brand" href="#">
    <img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-nt-white-transparent.png" width="50" height="50" class="d-inline-block align-middle mr-2" alt="">
    <span class="text-light align-middle navtext">AntrianQue</span>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse flex-row-reverse" id="navbarNav">
  	<div id="navbar-link-desktop">
  		<ul class="navbar-nav">
	    	<li class="nav-item mr-2">
	    		<a href="<?= base_url('merchant/auth/login')?>">
		        	<button type="button" class="btn btn-outline-light">Login</button>
		        </a>
	      	</li>
	      	<li class="nav-item">
	      		<a href="<?= base_url('merchant/auth/signup')?>">
		        	<button type="button" class="btn btn-light">Daftar</button>
		        </a>
	      	</li>
	    </ul>
	</div>
	<div id="navbar-link-mobile">
	    <ul class="navbar-nav mt-3">
	    	<li class="nav-item">
	        	<a href="<?= base_url('merchant/auth/login')?>" class="text-light">Login</a>
	      	</li>
	      	<li class="nav-item">
	      		<hr style="border-color: rgb(200,200,200);">
	        	<a href="<?= base_url('merchant/auth/signup')?>" class="text-light">Daftar</a>
	      	</li>
	    </ul>
    </div>
  </div>
</nav>
<!-- End of Navbar -->
<!-- Slide Section -->
<div id="slideLandingPage" class="carousel slide" data-ride="carousel">
 	<ol class="carousel-indicators">
    	<li data-target="#slideLandingPage" data-slide-to="0" class="active"></li>
    	<li data-target="#slideLandingPage" data-slide-to="1"></li>
    	<li data-target="#slideLandingPage" data-slide-to="2"></li>
	</ol>
  	<div class="carousel-inner slide-landing-page">
    	<div class="carousel-item active">
      		<div class="text-center slide-big p-2 bg-dark" id="slide-landing-page-1">
      			<div class="row h-100">
      				<div class="my-auto col-sm-12 text-light">
      					<span class="slide-title">Antrian Online Terbaik</span>
					  	<p>AntrianQue adalah layanan antrian online terbaik yang bisa anda gunakan untuk kebutuhan bisnis anda !</p> 
      				</div>
      			</div>
			</div>
    	</div>
    	<div class="carousel-item">
     		<div class="text-center slide-big bg-dark" id="slide-landing-page-2">
			  	<div class="row h-100">
      				<div class="my-auto col-sm-12 text-light">
      					<span class="slide-title">Permudah Bisnis Anda</span>
					  	<p>AntrianQue memberikan kemudahan dalam bisnis anda dengan teknologi internet. Bangun kualitas bisnis anda semakin baik dengan menggunakan AntrianQue - Antrian Online</p> 
      				</div>
      			</div>
			</div>
    	</div>
    	<div class="carousel-item">
      		<div class="text-center slide-big bg-dark" id="slide-landing-page-3">
			  	<div class="row h-100">
      				<div class="my-auto col-sm-12 text-light">
      					<span class="slide-title">Bangun Kenyamanan Pelanggan Anda</span>
					  	<p>Dengan antrian online, AntrianQue dapat membuat pelanggan anda nyaman tanpa perlu berdiri mengantri terlalu lama</p> 
      				</div>
      			</div>
			</div>
    	</div>
  	</div>
  	<a class="carousel-control-prev" href="#slideLandingPage" role="button" data-slide="prev">
    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    	<span class="sr-only">Previous</span>
  	</a>
  	<a class="carousel-control-next" href="#slideLandingPage" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    	<span class="sr-only">Next</span>
  	</a>
</div>
<!-- End of Slide Section -->
<div class="space" style="height: 30px"></div>

<div class="container-fluid">
	<div id="shortcut-selection">
		<!-- Shortcut Selection Section -->
		<div class="col-sm-12">
			<div class="row h-100 justify-content-center">
				<div class="col-2 box-mini text-center p-3 border-right">
					<div style="height: 320px;">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-about.png" width="130px" height="130px" class="m-2"><br>
						<span class="text-tosca"><b>AntrianQue</b></span><br>
						AntrianQue sebagai layanan antrian online berbasis aplikasi<br>
					</div>
					<button type="button" class="btn btn-outline-secondary scroll" data-target="#sectionAbout">Pelajari AntrianQue</button>
				</div>
				<div class="col-2 box-mini text-center p-3 border-right">
					<div style="height: 320px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-gear.webp" width="130px" height="130px" class="m-2"><br>
						<span><b>Cara Keja</b></span><br>
						Hanya dengan scan QR atau memasukan nomor PIN, pelanggan anda dapat langsung mengantri<br>
					</div>
					<button type="button" class="btn btn-outline-secondary scroll" data-target="#sectionProcedure">Lihat Cara Kerja</button>
				</div>
				<div class="col-2 box-mini text-center p-3 border-right">
					<div style="height: 320px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-merchant.png" width="130px" height="130px" class="m-2"><br>
						<span><b>Menjadi Merchant</b></span><br>
						Intergrasikan bisnis anda !. Gunakan AntrianQue sekarang dan beri kemudahan untuk pelanggan anda !.<br>
					</div>
					<button type="button" class="btn btn-outline-secondary scroll" data-target="#sectionMerchant">Menjadi Merchant</button>
				</div>
				<div class="col-2 box-mini text-center p-3">
					<div style="height: 320px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-contact.png" width="130px" height="130px" class="m-2"><br>
						<span><b>Hubungi Kami</b></span><br>
						Perlu bantuan? Hubungi kami melalui kontak yang tersedia.<br>
					</div>
					<button type="button" class="btn btn-outline-secondary scroll" data-target="#sectionContact">Kontak Kami</button>
				</div>
			</div>
		</div>
		<!-- End of Shortcut Selection Section -->
		<div class="space text-center" style="margin-top: 60px;height: 80px">
			<img src="<?= base_url().IMG_HTML_FOLDER?>divider-1.png" width="80%">
		</div>
	</div>

	<!-- Start of Introducing AntrianQue Section -->
	<div class="row" id="sectionAbout">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="mt-5 text-center">
						<img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-nt-blue.png" class="mr-3" width="70" height="70">
						<span class="h1 align-middle text-tosca"><b>AntrianQue</b></span>
					</div>
					<div class="mt-2 text-center">
						<h4 class="text-dark">efisien <i class="fa fa-circle align-middle" style="font-size: 10px"></i> simple <i class="fa fa-circle align-middle" style="font-size: 10px"></i> berkualitas</h4>
					</div>
				</div>
			</div>
			<div class="space" style="height: 50px"></div>
			<div class="row">
				<div class="col-md-9 mx-auto">
					<div class="row">
						<div class="col-md-5 p-2">
							<h4 class="font-weight-bold text-dark text-center text-md-left">Mengantri menjadi tidak membosankan !</h4>
							<p class="text-muted">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula. Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem.
							</p>
							<div class="text-center">
								<img src="<?= base_url().IMG_HTML_FOLDER?>antrianque-system-graphic.png" class="img-fluid" width="100%">
							</div>
							<div class="space" style="height: 30px"></div>
							<div class="col-12 text-center text-md-left mx-auto my-5 mx-md-0 my-md-0">
								<h5 class="font-weight-bold text-dark">Meningkatkan kualitas bisnis anda !</h5>
								<span><i class="fas fa-check text-success mr-3"></i>Lorem Ipsum Dolor Sit Amet</span><br>
								<span><i class="fas fa-check text-success mr-3"></i>Lorem Ipsum Dolor Sit Amet</span><br>
								<span><i class="fas fa-check text-success mr-3"></i>Lorem Ipsum Dolor Sit Amet</span><br>
								<span><i class="fas fa-check text-success mr-3"></i>Lorem Ipsum Dolor Sit Amet</span>
							</div>
						</div>
						<div class="col-md-7 px-5 text-center">
							<div id="slideAbout" class="carousel slide mt-lg-10" data-ride="carousel">
							  	<div class="carousel-inner">
							    	<div class="carousel-item active">
							      		<img class="d-block w-100" src="<?= base_url().IMG_FOLDER?>slide-landing-page/slide-about-1.png" alt="First slide">
							    	</div>
							    	<div class="carousel-item">
							      		<img class="d-block w-100" src="<?= base_url().IMG_FOLDER?>slide-landing-page/slide-about-2.png" alt="Second slide">
							    	</div>
							    	<div class="carousel-item">
							      		<img class="d-block w-100" src="<?= base_url().IMG_FOLDER?>slide-landing-page/slide-about-3.png" alt="Third slide">
							    	</div>
							  	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="space" style="height: 50px"></div>
			<div class="row">
				<div class="col-sm-12 bg-tosca p-4">
					<div class="row">
						<div class="col-md-7 mx-auto">
							<div class="row d-flex justify-content-between">
								<div class="col-md-5 m-2 text-center">
									<h3 class="font-weight-bold text-light">Mulai Antrian Online</h3>
									<div class="row" style="height: 250px">
										<img src="<?= base_url().IMG_HTML_FOLDER?>icon-merchant-white.png" alt="Mulai Antrian Onlinemu sekarang juga!" width="200px" height="200px" class="m-auto">
									</div>
									<div class="row" style="height: 100px">
										<p class="text-light">
											Intergrasikan bisnis anda sekarang dan jadikan antrian anda lebih mudah dan fleksibel menggunakan AntrianQue. Gratis !
										</p>
									</div>
									<a href="<?= base_url('merchant/auth/signup')?>">
										<button type="button" class="btn btn-outline-light">Daftar Sekarang</button>
									</a>
									<hr class="border border-light d-block d-sm-none">
								</div>
								<div class="col-md-5 m-2 text-center">
									<h3 class="font-weight-bold text-light">AntrianQue Mobile</h3>
									<div class="row" style="height: 250px">
										<img src="<?= base_url().IMG_HTML_FOLDER?>icon-get-on-playstore.png" alt="Dapatkan aplikasi AntrianQue di Platstore" class="m-auto img-fluid">
									</div>
									<div class="row" style="height: 100px">
										<p class="text-light">
											Anda juga dapat memulai AntrianQue menggunakan AntrianQue mobile sebagai Merchant atau User. Unduh sekarang juga ! 
										</p>
									</div>
									<button type="button" class="btn btn-outline-light">AntrianQue Mobile</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Start of How It's Work Section -->
	<div class="row" id="sectionProcedure">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-7 mx-auto text-center">
					<div class="mt-5 text-center">
						<span class="display-4 align-middle text-tosca"><i class="fas fa-cogs mr-3"></i>Cara Kerja AntrianQue</span>
						<hr>
					</div>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-md-9 mx-auto justify-content-between">
				<div class="col-sm-12 col-lg-5 text-center">
					<div class="row" style="height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-qrcode.png" alt="Scan Barcode dan dapatkan Antrian" width="200px" height="200px" class="m-auto">
					</div>
					<br>
					<h1>Scan Dengan QR Code</h1>
					<p class="text-muted"></p>
				</div>
				<div class="col-sm-12 col-lg-5 text-center">
					<div class="row" style="height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-pin.png" alt="Scan Barcode dan dapatkan Antrian" width="200px" height="200px" class="m-auto">
					</div>
					<br>
					<h1>Masukan Pin Merchant</h1>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-md-9 mx-auto justify-content-between">
				<div class="col-12 text-center">
					<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula. Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem.</p>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-md-9 mx-auto justify-content-between">
				<div class="col-sm-12 col-lg-5 text-center">
					<div class="row" style="height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-internet.png" alt="Scan Barcode dan dapatkan Antrian" width="200px" height="200px" class="m-auto">
					</div>
					<br>
					<h1>Antrian Secara Online</h1>
					<p class="text-muted"></p>
				</div>
				<div class="col-sm-12 col-lg-5 text-center">
					<div class="row" style="height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-phone-vibrate.png" alt="Scan Barcode dan dapatkan Antrian" width="300px" height="200px" class="m-auto">
					</div>
					<br>
					<h1>Notifikasi Pelanggan Anda</h1>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-md-9 mx-auto justify-content-between">
				<div class="col-12 text-center">
					<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula. Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- Start of Join As Merchant Section -->
	<div class="row" id="sectionMerchant">
		<div class="col-sm-12 bg-light-gray">
			<div class="row">
				<div class="col-sm-7 mx-auto text-center">
					<div class="mt-5 text-center">
						<img class="mr-3" src="<?= base_url().IMG_HTML_FOLDER?>icon-merchant.png" width="50px" height="50px">
						<span class="display-4 align-middle">Menjadi Merchant</span>
						<hr>
					</div>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-sm-10 mx-auto justify-content-between">
				<div class="col-sm-12 col-lg-3 text-center">
					<div class="row rounded-circle mx-auto bg-white" style="width: 200px;height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-free.png" alt="Scan Barcode dan dapatkan Antrian" width="100px" height="100px" class="m-auto">
					</div>
					<br>
					<h3>Daftar & Gunakan Gratis !</h3>
					<div class="text-center" style="min-height: 200px">
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue.</p>
					</div>
				</div>
				<div class="col-sm-12 col-lg-3 text-center">
					<div class="row rounded-circle mx-auto bg-white" style="width: 200px;height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-clock.png" alt="Scan Barcode dan dapatkan Antrian" width="100px" height="100px" class="m-auto">
					</div>
					<br>
					<h3>Dapat Digunakan 24/7</h3>
					<div class="text-center" style="min-height: 200px">
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue.</p>
					</div>
				</div>
				<div class="col-sm-12 col-lg-3 text-center">
					<div class="row rounded-circle mx-auto bg-white" style="width: 200px;height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-qrcode.png" alt="Scan Barcode dan dapatkan Antrian" width="100px" height="100px" class="m-auto">
					</div>
					<br>
					<h3>Bagikan Akses Pelanggan</h3>
					<div class="text-center" style="min-height: 200px">
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue.</p>
					</div>
				</div>
				<div class="col-sm-12 col-lg-3 text-center">
					<div class="row rounded-circle mx-auto bg-white" style="width: 200px;height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-statistic.png" alt="Scan Barcode dan dapatkan Antrian" width="100px" height="100px" class="m-auto">
					</div>
					<br>
					<h3>Lihat Statistik Antrian</h3>
					<div class="text-center" style="min-height: 200px">
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue.</p>
					</div>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-md-9 mx-auto justify-content-between">
				<div class="col-sm-12 col-lg-5 text-center">
					<div class="row" style="height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-pc.png" alt="Scan Barcode dan dapatkan Antrian" width="200px" height="200px" class="m-auto">
					</div>
					<br>
					<h1>Kontrol Lewat Website</h1>
					<p class="text-muted"></p>
				</div>
				<div class="col-sm-12 col-lg-5 text-center">
					<div class="row" style="height: 200px">
						<img src="<?= base_url().IMG_HTML_FOLDER?>icon-phone.png" alt="Scan Barcode dan dapatkan Antrian" width="200px" height="200px" class="m-auto">
					</div>
					<br>
					<h1>Kontrol Lewat Smartphone</h1>
				</div>
			</div>
			<div class="space" style="height: 30px"></div>
			<div class="row col-md-9 mx-auto justify-content-between">
				<div class="col-12 text-center">
					<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet sapien ultricies, semper quam in, euismod turpis. Praesent faucibus id lacus eu suscipit. Phasellus a turpis augue. Donec id sollicitudin ligula. Curabitur tempus odio eu vehicula ultricies. Praesent id felis viverra, maximus sem quis, facilisis nunc. Aliquam molestie gravida lorem. Vestibulum et facilisis lorem.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- Start of Contact Us Section -->
	<div class="row" id="sectionContact">
		<div class="col-12 col-lg-6">
			<div class="row col-sm-10 mx-auto">
				<div class="col-12">
					<div class="space" style="height: 60px"></div>
					<h2 class="align-middle"><i class="fas fa-phone-square"></i> Hubungi Kami</h2>
					<p class="text-muted">Butuh bantuan ? Silahkan isi formulir di bawah ini dan tanyakan sesuatu kepada tim AntrianQue. Kami akan menghubungi anda melalui email</p>
					<?php echo form_open('home/contact_form'); ?>
					<div class="form-group">
						<span class="font-weight-bold">Nama :</span><br>
						<input type="text" name="name" class="form-control border-dark" placeholder="Masukan Nama">
					</div>
					<div class="form-group">
						<span class="font-weight-bold">Email :</span><br>
						<input type="email" name="email" class="form-control border-dark" placeholder="Masukan Email">
					</div>
					<div class="form-group">
						<span class="font-weight-bold">Pesan :</span><br>
						<textarea name="message" class="form-control border-dark" rows="5" placeholder="Isi Pesan"></textarea>
					</div>
					<div class="form-group text-right">
						<input type="submit" class="btn btn-dark" value="Kirim Pesan">
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6 bg-tosca">
			<div class="row col-sm-10 mx-auto">
				<div class="col-12">
					<div class="space" style="height: 60px"></div>
					<h2 class="align-middle text-light"><i class="fas fa-address-book"></i> Kontak Kami</h2>
					<br><br>
					<a href="https://www.facebook.com/antrianque/" target="_blank">
						<button type="button" class="col-12 col-lg-7 text-left btn btn-outline-light btn-contact-landing-page" style="border-radius: 20px">
							<span class="h4"><i class="fab fa-facebook-square mr-3"></i> AntrianQue</span>
						</button>
					</a>
					<br><br>
					<a href="https://www.twitter.com/antrianque/" target="_blank">
						<button type="button" class="col-12 col-lg-7 text-left btn btn-outline-light  btn-contact-landing-page" style="border-radius: 20px">
							<span class="h4"><i class="fab fa-twitter-square mr-3"></i> @antrianque</span>
						</button>
					</a>
					<br><br>
					<a href="https://www.instagram.com/antrianque/" target="_blank">
						<button type="button" class="col-12 col-lg-7 text-left btn btn-outline-light  btn-contact-landing-page" style="border-radius: 20px">
							<span class="h4"><i class="fab fa-instagram mr-3"></i> @antrianque</span>
						</button>
					</a>
					<br><br>
					<a href="mailto:cs@antrianque.com">
						<button type="button" class="col-12 col-lg-7 text-left btn btn-outline-light  btn-contact-landing-page" style="border-radius: 20px">
							<span class="h4"><i class="far fa-envelope mr-3"></i> cs@antrianque.com</span>
						</button>
					</a>
					<br><br>
					<a href="https://wa.me/6281383084990?text=saya%20ingin%20bertanya%20tentang%20AntrianQue" target="_blank">
						<button type="button" class="col-12 col-lg-7 text-left btn btn-outline-light  btn-contact-landing-page" style="border-radius: 20px">
							<span class="h4"><i class="fab fa-whatsapp mr-3"></i> +6281383084990</span>
						</button>
					</a>
					<div class="space" style="height: 60px"></div>
				</div>
			</div>
		</div>
	</div>
</div>