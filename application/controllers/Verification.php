<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verification extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_nofollow'] = true;
		$this->load->library('verification_lib');
	}

	public function verify($id_verification) {
		if  ($id_verification){
			$this->verification_lib->verify_by_token($id_verification);
		}
	}

}
