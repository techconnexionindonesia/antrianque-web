<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_nofollow'] = true;
		$this->load->library('merchant_lib');
		$this->load->model('merchant_model');
		$this->load->model('merchant_profile_model');
	}

	public function index() {
		$this->page_criteria('active');
		$this->data['title'] = "AntrianQue - Profile";
		$this->data['merchant_data'] = $this->merchant_lib->get_merchant_data();
		$this->data['custom_js'] = array('merchant/profile/profile');
		$this->page_display->display_merchant('profile',$this->data);
	}

	public function do_update() {
		/* For ajax Function */
		$result['status'] = 'success';
		if ($this->input->post()){
			$id_merchant = $this->get_merchant_session_id();
			$criteria_profile = $this->get_criteria();
			$criteria_merchant['time_zone'] = $this->input->post('time_zone');
			if ($this->input->post('old_password') && $this->input->post('new_password') && $this->input->post('confirm_new_password')){
				$is_confirm_password_match = $this->input->post('new_password') == $this->input->post('confirm_new_password');
				if ($this->is_old_password_match($this->input->post('old_password')) && $is_confirm_password_match){
					$criteria_merchant['password'] = sha1($this->input->post('new_password'));
				} else if (!$this->is_old_password_match($this->input->post('old_password'))){
					$result['status'] = 'wrong_password';
				} else {
					$result['status'] = 'failed';
				}
			}
			if (!empty($criteria_profile) && $result['status'] == 'success'){
				$this->merchant_profile_model->update($criteria_profile,array('id_merchant' => $id_merchant));
			}
			if (!empty($criteria_merchant) && $result['status'] == 'success'){
				$this->merchant_model->update($criteria_merchant,$id_merchant);
				$this->session->set_userdata('time_zone', $criteria_merchant['time_zone']);
			}
			$result['redirect'] = base_url('merchant');
		} else {
			$result['status'] = 'failed';
		}
		echo json_encode($result);
	}

	private function get_criteria(){
		$criteria = array();
		if ($this->is_loged_merchant() && $this->input->post()){
			$criteria = array(
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
				'address_line1' => $this->input->post('address_line1'),
				'address_line2' => $this->input->post('address_line2'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'postal_code' => $this->input->post('postal_code')
			);
		}
		return $criteria;
	}

	private function is_old_password_match($password){
		$result = false;
		if ($password){
			$id_merchant = $this->get_merchant_session_id();
			$criteria = array(
				'id_merchant' => $id_merchant,
				'password' => sha1($password)
			);
			$data_merchant = $this->merchant_model->get($criteria);
			$data_merchant = reset($data_merchant);
			if ($data_merchant){
				$result = true;
			}
		}
		return $result;
	}
}