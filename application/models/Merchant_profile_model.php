<?php

class Merchant_profile_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= MERCHANT_PROFILE_TABLE;
    protected $_primary_key = MERCHANT_PROFILE_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}