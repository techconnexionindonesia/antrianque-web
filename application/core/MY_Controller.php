<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('merchant_model');
	}

	public function is_loged_merchant(){
		if ($this->session->userdata('id_merchant')){
			return true;
		} else {
			return false;
		}
	}

	public function page_criteria($criteria){
		$redirect = false;
		switch ($criteria) {
			case 'loged':
				if (!$this->is_loged_merchant())
					$redirect = true;
				break;

			case 'not_loged':
				if ($this->is_loged_merchant())
					$redirect = true;
				break;

			case 'active':
				$merchant_status = $this->get_merchant_session_status();
				if ($merchant_status != 'active')
					$redirect = true;
				break;

			case 'pending':
				$merchant_status = $this->get_merchant_session_status();
				if ($merchant_status != 'pending')
					$redirect = true;
				break;
		}

		if ($redirect){
			if ($this->is_loged_merchant()) {
				$merchant_status = $this->get_merchant_session_status();
				if ($merchant_status == 'active') {
					redirect('merchant/dashboard');
				} else if ($merchant_status == 'pending') {
					redirect('merchant/auth/pending');
				} else {
					redirect('merchant/auth/login');
				}
			} else {
				redirect('merchant/auth/login');
			}
		}
	}

	public function get_merchant_session_id() {
		if ($this->is_loged_merchant()){
			return $this->session->userdata('id_merchant');
		} else {
			return false;
		}
	}

	public function get_merchant_session_status() {
		if ($this->is_loged_merchant()){
			$id_merchant = $this->session->userdata('id_merchant');
			$data_merchant = $this->merchant_model->get($id_merchant);
			$data_merchant = reset($data_merchant);
			$merchant_status = $data_merchant['status'];
			return $merchant_status;
		} else {
			return false;
		}
	}

	/** --- Centrailized CURL function --- **/
	public function do_curl($url, $header = false, $data = false, $method = false){
	    $ch = curl_init();

	    if ($header)
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	    if ($data){
	        $data_to_post = http_build_query($data);
	    }
	    else $data_to_post = null;

	    if ($method == "GET" && $data){
	        curl_setopt($ch, CURLOPT_URL, $url.'?'.$data_to_post);
	    } else {
	        curl_setopt($ch, CURLOPT_URL, $url);
	    }

	    curl_setopt($ch, CURLOPT_HEADER, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
	    
	    if ($method == 'POST' && $data_to_post){
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_to_post);
	    }

	    $output = curl_exec($ch); 

	    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	    $result['data'] = substr($output, $header_size);
	    $result['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	    curl_close($ch);      
	    return $result;
	}

}
