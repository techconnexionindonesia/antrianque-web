/*
 * Core Function
 * AntrianQue Web
 *
 */
var core = function(){};
$(document).ready(function(){
	core = function(){
		
		$('.scroll').click(function(){
			var target_scroll = $(this).data('target');
			$('html, body').animate({
		        scrollTop: $(target_scroll).offset().top
		    }, 1000);
		});

        $('[data-toggle="tooltip"]').tooltip();
        $('.tool-tip').tooltip();

	}();
});

function do_ajax_post_sync(url,data,type,dataType){
    data[ajax_secure_csrf_name] = ajax_secure_csrf_value;
    return $.ajax(url, {
        type: type,  // http method
        data: data,  // data to submit
        dataType: dataType,
        async: false,
        success: function (result) {
            return result;
        },
        error: function (jqXhr, textStatus, errorMessage) {
            return errorMessage;
        }
    });
}

function do_ajax_post(url,data,type,dataType, callback){
    data[ajax_secure_csrf_name] = ajax_secure_csrf_value;
    $.ajax(url, {
        type: type,  // http method
        data: data,  // data to submit
        dataType: dataType,
        success: function (result) {
        	callback(false,result);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            callback(errorMessage,false);
        }
    });
}

function get_response_ajax(response , type){
	if (type == 'json' || type == 'JSON'){
	    return response.responseJSON;
	} else if (type == 'text'){
		return response.responseText;
	}
}

function get_val_by_name(name,element_name){
    if (name){
        if (!element_name){
            element_name = '*';
        }
        return $(element_name+'[name="'+name+'"]').val();
    } else {
        return false;
    }
}


function hide_all(elements){
    elements.forEach(function(element,index){
        $(element).hide();
    });
}

function show_alert_success(optional_text,optional_second){
    if (optional_text){
        $('#alert_success_custom_text').text(optional_text);
        $('#alert_success_custom_text').show();
        $('#alert_success_default_text').hide();
    } else {
        $('#alert_success_custom_text').hide();
        $('#alert_success_default_text').show();
    }
    optional_second = optional_second ? optional_second : default_alert_show_second;
    $('#alert_success').show('slow');
    setTimeout(function(){
        $('#alert_success').hide('slow');
    },/*second to hide (ms): */ optional_second * 1000);
}

function show_alert_failed(optional_text,optional_second){
    if (optional_text){
        $('#alert_failed_custom_text').text(optional_text);
        $('#alert_failed_custom_text').show();
        $('#alert_failed_default_text').hide();
    } else {
        $('#alert_failed_custom_text').hide();
        $('#alert_failed_default_text').show();
    }
    optional_second = optional_second ? optional_second : default_alert_show_second;
    $('#alert_failed').show('slow');
    setTimeout(function(){
        $('#alert_failed').hide('slow');
    },/*second to hide (ms): */ optional_second * 1000);
}

function show_loading(){
    $('#alert_loading').show();
}

function hide_loading(){
    $('#alert_loading').hide();
}

function redirect(url){
    if (url){
        window.location.href = url;
    } else {
        window.location.href = base_url;
    }
}

function enable(element){
    if (element){
        $(element).prop('disabled',false);
    }
}

function disable(element){
    if (element){
        $(element).prop('disabled',true);
    }
}