<?php 
$section = $this->uri->segment(1);

$is_non_footer = false;

if ($section == 'merchant'){
    $non_footer_page_merchant = $this->config->item('non_footer_page_merchant');
    foreach ($non_footer_page_merchant as $page) {
        $uri = explode("/", $page);
        $uri_length = count($uri);
        $count = 0;
        foreach ($uri as $uri_number => $single_uri) {
            if ($single_uri == $this->uri->segment($uri_number + 2)){
                $count++;
            }

            if ($count == $uri_length){
                $is_non_footer = true;
            }
        }
    }

    $status_dom = isset($queue_available) && $queue_available ? array(
        'class'=>'footer-status-on',
        'value'=>'on') : 
        array('class'=>'footer-status-off','value'=>'off');
}
?>

<?php if ($section == 'admin' && $is_non_footer == false){?>

    <!-- Footer Admin di sini -->

<?php } else if ($section == 'merchant' && $is_non_footer == false) {?>

<div class="footer-merchant bg-tosca">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row mx-auto">
                    <!-- AntrianQue Logo -->
                    <div class="col-3 col-lg-2 text-center">
                        <div class="row h-100">
                            <div class="m-auto">
                                <div class="row h-100">
                                    <img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-nt-white-transparent.png" width="40" height="40" class="d-inline-block align-middle mr-2" alt="">
                                    <span class="text-light h5 font-weight-bold m-auto d-none d-lg-block">AntrianQue</span><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Tools Tab -->
                    <div class="col-lg-7 text-light d-none d-lg-block">
                        <div class="row h-100">
                            <div class="col-lg-3 h-100">
                                <div class="row h-100">
                                    <span class="m-auto clock-display"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 h-100">
                                <div class="row h-100">
                                    <div class="col-12 m-auto">
                                        <div class="row h-100">
                                            <div class="row col-10 justify-content-end">
                                                <span class="footer-status <?=$status_dom['class']?> mr-1" data-toggle="tooltip" data-placement="top" title="Aktif/Nonaktifkan Antrian" data-value="<?=$status_dom['value']?>"></span>
                                            </div>
                                            <div class="row col-2">
                                                <i id="footer_status_loading" class="my-auto fa fa-spinner fa-spin" style="display: none"></i>
                                                <i id="footer_status_success" class="my-auto fa fa-check" style="display: none"></i>
                                                <i id="footer_status_failed" class="my-auto fa fa-times" style="display: none"></i>
                                            </div>
                                            <audio id="footer_status_audio" src="<?=base_url().AUDIO_FOLDER?>footer/footer-toogle-status.ogg"></audio>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 h-100">
                                <div class="row h-100">
                                    <div class="m-auto">
                                        <div class="row h-100">
                                            <span class="footer-queue my-auto mr-2" data-toggle="tooltip" data-placement="top" title="Jumlah Antrian Hari Ini"></span>
                                            <span class="my-auto" id="count_queue">50</span><span class="my-auto">/1.000</span>
                                        </div>
                                    </div>
                                    <audio id="footer_queue_audio" src="<?=base_url().AUDIO_FOLDER?>footer/footer-click-reset.ogg"></audio>
                                </div>
                            </div>
                            <div class="col-lg-3 h-100">
                                <div class="row h-100">
                                    <div class="col-12 m-auto">
                                        <div class="row h-100">
                                            <div class="row col-10 justify-content-end">
                                                <?php if (count($queue_numbering) > 1){?>
                                                    <span class="footer-reset mr-2" data-placement="top" title="Reset Antrian" id="footerResetOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
                                                    <div class="dropdown-menu" aria-labelledby="footerResetOption">
                                                        <?php foreach ($queue_numbering as $numbering) {?>
                                                        <a class="dropdown-item cursor-pointer footer-reset-item bg-light" data-target="<?=isset($numbering) ? $numbering['prefix'] : ''?>">Reset <?=isset($numbering) ? $numbering['prefix'] : ''?></a>
                                                        <?php }?>
                                                    </div>
                                                <?php } else {?>
                                                    <span class="footer-reset mr-2 footer-reset-item" data-toggle="tooltip" data-placement="top" title="Reset Antrian"></span>
                                                <?php }?>
                                            </div>
                                            <div class="row col-2">
                                                <i id="footer_reset_loading" class="my-auto fa fa-spinner fa-spin" style="display: none"></i>
                                                <i id="footer_reset_success" class="my-auto fa fa-check" style="display: none"></i>
                                                <i id="footer_reset_failed" class="my-auto fa fa-times" style="display: none"></i>
                                            </div>
                                            <audio id="footer_reset_audio" src="<?=base_url().AUDIO_FOLDER?>footer/footer-click-reset.ogg"></audio>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Help tab -->
                    <div class="col-9 col-lg-3 text-light">
                        <div class="row h-100">
                            <div class="col-12 m-auto">
                                <div class="row h-100 justify-content-end">
                                    <button class="btn btn-outline-light mr-5" data-placement="top" id="footerMenuNavigation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu <i class="far fa-arrow-alt-circle-up ml-2"></i></button>
                                    <div class="dropdown-menu" aria-labelledby="footerMenuNavigation">
                                        <a class="dropdown-item cursor-pointer" href="<?=base_url('merchant/intro')?>"><i class="fa fa-question mr-2"></i>Panduan</a>
                                        <a class="dropdown-item cursor-pointer" href="<?=base_url('terms_and_contions')?>"><i class="fas fa-check mr-2"></i>Syarat dan ketentuan</a>
                                        <a class="dropdown-item cursor-pointer" href="<?=base_url('privacy_policy')?>"><i class="fas fa-shield-alt mr-2"></i>Kebijakan Privasi</a>
                                    </div>
                                    <span class="footer-cs cursor-pointer my-auto tool-tip" data-toggle="modal" data-target="#csModal" data-placement="top" title="Customer Support"></span>
                                    <span class="col-1 d-none d-lg-block">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="csModal" tabindex="-1" role="dialog" aria-labelledby="csModal" aria-hidden="true">
    <div class="row h-100 modal-dialog modal-lg" role="document">
        <div class="col-12 m-auto bg-white modal-content border-radius-10">
            <div class="row bg-light-gray cs-banner" style="height: 350px">
                
            </div>
            <br>
            <div class="row">
                <div class="col-lg-8 m-auto text-center">
                    <h3 class="text-muted">Terhubung dengan kami, AntrianQue siap membantu anda</h3>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-8 m-auto text-center">
                    <textarea class="form-control" rows="5" id="cs_report_message" placeholder="Apa yang bisa AntrianQue lakukan untuk anda?"></textarea>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-lg-8 m-auto text-right">
                    <a class="text-primary cursor-pointer mr-2" data-dismiss="modal">Batal</a>
                    <button id="cs_submit" class="btn btn-primary">Kirim</button>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<?php } else if (!$is_non_footer) {?>

<!-- Start of Contact Us Section -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12 bg-darker">
            <div class="row col-lg-10 mx-auto">
                <!-- AntrianQue Logo -->
                <div class="col-12 col-lg-6 py-5 text-center">
                    <img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-nt-white-transparent.png" width="70" height="70" class="d-inline-block align-middle mr-2" alt="">
                    <span class="text-light align-middle display-4 d-none d-lg-inline-block font-weight-bold">AntrianQue</span>
                    <span class="text-light align-middle h2 d-lg-none font-weight-bold">AntrianQue</span>
                    <h4 class="text-light d-none d-lg-block">efisien <i class="fa fa-circle align-middle" style="font-size: 10px"></i> simple <i class="fa fa-circle align-middle" style="font-size: 10px"></i> berkualitas</h4>
                    <h5 class="text-light d-block d-lg-none">efisien <i class="fa fa-circle align-middle" style="font-size: 10px"></i> simple <i class="fa fa-circle align-middle" style="font-size: 10px"></i> berkualitas</h5>

                </div>
                <!-- Explore Tab -->
                <div class="col-12 col-lg-6 p-lg-5 text-light">
                    <h3 class="font-weight-bold">Explore Us !</h3>
                    <br>
                    <a href="<?= base_url('home/pricing')?>">
                        <span class="text-light"><i class="fas fa-angle-right mr-4"></i> HARGA LAYANAN</span>
                    </a>
                    <br>
                    <a href="<?= base_url('home/terms_and_contions')?>">
                        <span class="text-light"><i class="fas fa-angle-right mr-4"></i> SYARAT DAN KETENTUAN</span>
                    </a>
                    <br>
                    <a href="<?= base_url('home/privacy_policy')?>">
                        <span class="text-light"><i class="fas fa-angle-right mr-4"></i> KEBIJAKAN PRIVASI</span>
                    </a>
                    <br><br>
                    <div class="text-center text-lg-left">
                        <a href="<?= base_url('merchant/auth/login')?>">
                            <button type="button" class="btn btn-outline-light">Login</button>
                        </a>
                        &nbsp;
                        <a href="<?= base_url('merchant/auth/signup')?>">
                            <button type="button" class="btn btn-light">Daftar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row col-lg-10 mx-auto">
                <div class="col-12 text-center text-muted">
                    <hr class="border border-white border-sm">
                    <span class="font-weight-light">AntrianQue &copy; 2020</span><br>
                    <span class="font-weight-light"><small>AntrianQue merupakan produk layanan dari Tech Connexion Indonesia. Dikembangkan dan dijalankan oleh Tech Connexion Indonesia</small></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>

        <script src="<?= $component_folder.VENDOR_JS_FOLDER.'jquery-3.4.1.min.js'?>"></script>
        <script src="<?= $component_folder.VENDOR_JS_FOLDER.'bootstrap.bundle.min.js'?>"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

        
        <script src="<?= $component_folder.JS_FOLDER;?>constants.js"></script>
        <script src="<?= $component_folder.JS_FOLDER;?>core.js"></script>
        <script src="<?= $component_folder.JS_FOLDER;?>footer.js"></script>
        <script src="<?= $component_folder.JS_FOLDER;?>clock.js"></script>

        <!-- Custom JS -->
        <?php if (isset($custom_js)){
        foreach ($custom_js as $js){?>
        <script type="text/javascript" src="<?= $component_folder.JS_FOLDER.$js.'.js'?>"></script>
        <?php }}?>

        <!-- Custom Vendor JS -->
        <?php if (isset($custom_vendor_js)){
        foreach ($custom_js as $js){?>
        <script type="text/javascript" src="<?= $component_folder.VENDOR_JS_FOLDER.$js.'.js'?>"></script>
        <?php }}?>

        <script type="text/javascript">
            var ajax_secure_csrf_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var ajax_secure_csrf_value = '<?php echo $this->security->get_csrf_hash(); ?>';
        </script>

    </body>
</html>