<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class Merchant_activity_logger extends MY_Controller{
 
	public function __construct() { 
		$this->_CI = &get_instance();
        $this->_CI->load->model('merchant_activity_model');
    }

    public function log($activity) {
        if ($this->_CI->is_loged_merchant()){
            $criteria = $this->set_log_criteria($activity);
            $this->_CI->merchant_activity_model->insert($criteria);
        }
    }

    private function set_log_criteria($activity) {
        $id_merchant = $this->_CI->session->userdata('id_merchant');
        return array(
                'id_merchant' => $id_merchant,
                'activity' => $activity
            );
    }
 
}
 