<div class="container-fluid">
	<div class="row vh-100">
		<div class="col-12 bg-tosca">
			<div class="row h-100">
				<div class="col-10 col-lg-10 m-auto p-4 bg-light border rounded text-center">
					<span class="h2">Pilih Paket Langganan</span>
					<hr><br>
					<div class="row justify-content-center">
						<!-- Free Subscription Section -->
						<div class="col-10 col-lg-3 mx-2 my-3 border border-secondary rounded shadow-soft">
							<!-- Header Title -->
							<div class="row bg-primary" style="height: 140px">
								<div class="col-12 px-2 py-4 text-center my-auto">
									<span class="h1 text-light">Paket Gratis</span>
								</div>
							</div>
							<!-- End of Header Title -->
							<!-- Description -->
							<div class="row">
								<div class="col-12 p-3">
									<div class="text-left">
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Durasi Langganan : </span>Selamanya</b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Maksimal Antrian : </span><?= SUBSCRIPTION_FREE_MAX_QUEUE?> / Hari</b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Dual Platform (Web & Mobile)</span></b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Customer Support</span></b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Kontrol Akses Dashboard</span></b></label><br>
									</div>
									<br>
									<hr>
									<!-- Price Section -->
									<label class="h2 text-success"><b>GRATIS</b></label><br>
									<span class="text-muted">(Paket yang sedang anda gunakan)</span>
									<!-- End of Price Section -->
								</div>
							</div>
							<!-- End of Description -->
						</div>
						<!-- End of Free Subscription Section -->
						<?php foreach ($subscriptions as $subscription) {?>
						<div class="col-10 col-lg-3 mx-2 my-3 border border-secondary rounded shadow-soft">
							<!-- Header Title -->
							<div class="row bg-primary" style="height: 140px">
								<div class="col-12 px-2 py-4 text-center my-auto">
									<span class="h1 text-light"><?= $subscription['name']?></span>
								</div>
							</div>
							<!-- End of Header Title -->
							<!-- Description -->
							<div class="row">
								<div class="col-12 p-3">
									<div class="text-left">
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Durasi Langganan : </span><?= $subscription['duration']?> Hari</b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Maksimal Antrian : </span><?= number_format($subscription['max'],false,',','.')?> / Hari</b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Dual Platform (Web & Mobile)</span></b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Customer Support</span></b></label><br>
										<label><i class="fas fa-check text-success mr-3"></i><b><span class="text-tosca">Kontrol Akses Dashboard</span></b></label><br>
									</div>
									<br>
									<hr>
									<!-- Price Section -->
									<?php if ($subscription['discount']){?>
									<label class="h5 text-muted"><del>Rp.<?= number_format($subscription['price'],0,',','.')?></del></label><br>
									<label class="h2 text-success"><b>Rp.<?= number_format($subscription['price_after_discount'],0,',','.')?></b></label>
									<?php } else {?>
									<label class="h2 text-success"><b>Rp.<?= number_format($subscription['price'],0,',','.')?></b></label>
									<?php }?>
									<!-- End of Price Section -->
								</div>
							</div>
							<!-- End of Description -->
						</div>
						<?php }?>
					</div>
					<div class="space" style="height: 50px;">
					</div>
					<a href="<?= base_url().'merchant/subscription'?>">
						<button class="col-12 col-lg-3 btn btn-outline-dark mr-3 mb-3">Beli Paket</button>
					</a>
					<a href="<?= base_url().'merchant/dashboard'?>">
						<button class="col-12 col-lg-3 btn btn-primary mb-3">Lewati</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>