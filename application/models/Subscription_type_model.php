<?php

class Subscription_type_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= SUBSCRIPTION_TYPE_TABLE;
    protected $_primary_key = SUBSCRIPTION_TYPE_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}