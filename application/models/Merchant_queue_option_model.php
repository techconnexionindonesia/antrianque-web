<?php

class Merchant_queue_option_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= MERCHANT_QUEUE_OPTION_TABLE;
    protected $_primary_key = MERCHANT_QUEUE_OPTION_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}