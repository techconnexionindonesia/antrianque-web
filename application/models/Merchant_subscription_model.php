<?php

class Merchant_subscription_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= MERCHANT_SUBSCRIPTION_TABLE;
    protected $_primary_key = MERCHANT_SUBSCRIPTION_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    public function get_merchant_active_subscription($id_merchant){
    	$this->db->select('*');
    	$this->db->where("subscription_start <= CURDATE() 
    		AND subscription_expired >= CURDATE() 
    		AND disabled = 0 
    		AND cancelled_by_merchant = 0
    		AND id_merchant = ".$id_merchant);
    	$subscription = parent::get();
    	$subscription = reset($subscription);
    	return $subscription;
    }

}