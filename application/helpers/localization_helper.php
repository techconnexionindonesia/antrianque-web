<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function local($format, $date_data = false, $option = false){
	$interval = load_interval_time_zone();
	if (!is_string($date_data) && !is_bool($date_data)){
		$date_data = date('Y-m-d H:i:s',$date_data);
	}
	if (!$option){
		$option = "+ 1 hours";
	}
	$date = $date_data ? date($format, strtotime('+ '.$interval.' hours', strtotime($option,strtotime($date_data)))) : date($format, strtotime('+ '.$interval.' hours', strtotime($option,date('Y-m-d H:i:s'))));
	return $date;
}

function load_interval_time_zone(){
	$CI = & get_instance();
	$CI->load->library('session');
	$time_zone = $CI->session->userdata('time_zone');
	$interval = ((int) $time_zone) - SERVER_GMT;
	return $interval;
}