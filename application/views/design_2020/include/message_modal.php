<!-- Alert Success -->
<div class="alert-fixed col-11 col-lg-4" id="alert_success" style="display: none;">
	<div class="row">
		<div class="col-2 text-center bg-success" style="border-top-left-radius: 10px;border-bottom-left-radius: 10px">
			<div class="row h-100">
				<div class="m-auto">
					<i class="fas fa-check text-light" style="font-size: 30px"></i>
				</div>
			</div>
		</div>
		<div class="col-10 bg-light" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
			<div class="row h-100">
				<div class="my-auto text-secondary p-2">
					<span id="alert_success_default_text">Permintaan anda berhasil diproses</span>
					<span id="alert_success_custom_text" style="display: none"></span>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Alert Failed -->
<div class="alert-fixed col-11 col-lg-4" id="alert_failed" style="display: none">
	<div class="row">
		<div class="col-2 text-center bg-danger" style="border-top-left-radius: 10px;border-bottom-left-radius: 10px">
			<div class="row h-100">
				<div class="m-auto">
					<i class="fas fa-times text-light" style="font-size: 30px"></i>
				</div>
			</div>
		</div>
		<div class="col-10 bg-light" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
			<div class="row h-100">
				<div class="my-auto text-secondary p-2">
					<span id="alert_failed_default_text">Permintaan anda tidak dapat diproses</span>
					<span id="alert_failed_custom_text" style="display: none"></span>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Alert Loading -->
<div class="alert-fixed col-11 col-lg-4" id="alert_loading" style="display: none">
	<div class="row">
		<div class="col-2 text-center bg-green-custom" style="border-top-left-radius: 10px;border-bottom-left-radius: 10px">
			<div class="row h-100">
				<div class="m-auto">
					<i class="fas fa-spinner fa-pulse text-light" style="font-size: 30px"></i>
				</div>
			</div>
		</div>
		<div class="col-10 bg-light" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
			<div class="row h-100">
				<div class="my-auto text-dark p-2">
					<span>Permintaan anda sedang diproses</span>
				</div>
			</div>
		</div>
	</div>
</div>