<div class="container">
	<div class="row">
		<div class="col-12 col-lg-8 mx-auto">
			<div class="space" style="height: 60px"></div>
			<h2 class="text-tosca"><i class="fas fa-user mr-3"></i>Edit Profil</h2>
			<br>
			<div class="form-group">
				<section id="invalidSection" style="display: none;">
					<div class="col-12 label-info-danger" id="invalidDiv">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Tidak dapat melakukan update. Terdapat data yang tidak valid
					</div>
					<br><br>
				</section>
				<span class="text-muted">Detail Akun</span>
				<hr>
				<div class="row">
					<div class="col-12">
						<label>Nama : </label>
						<input type="text" class="form-control" name="name" placeholder="Nama Merchant" value="<?= isset($merchant_data['name']) ? $merchant_data['name'] : ''?>">
						<span class="text-danger" id="invalidName" style="display: none;"><small>Nama tidak valid</small></span>
					</div>
				</div>
				<div class="space" style="height: 30px"></div>
				<div class="row">
					<div class="col-12">
						<label>Deskripsi Merchant : </label>
						<textarea class="form-control" name="description" placeholder="Deskripsi tentang Merchant" rows="3"><?= isset($merchant_data['description']) ? $merchant_data['description'] : ''?></textarea>
						<span class="text-danger" id="invalidDescription" style="display: none;"><small>Deskripsi harus diisi</small></span>
					</div>
				</div>
				<div class="space" style="height: 30px"></div>
				<div class="row">
					<div class="col-12">
						<label>Alamat : </label>
						<input type="text" class="form-control" name="address_line1" placeholder="Alamat baris 1" value="<?= isset($merchant_data['address_line1']) ? $merchant_data['address_line1'] : ''?>" style="margin-bottom: 10px">
						<input type="text" class="form-control" name="address_line2" placeholder="Alamat baris 2 (opsional)" value="<?= isset($merchant_data['address_line2']) ? $merchant_data['address_line2'] : ''?>" style="margin-bottom: 10px">
						<input type="text" class="form-control" name="city" placeholder="Kota" value="<?= isset($merchant_data['city']) ? $merchant_data['city'] : ''?>" style="margin-bottom: 10px">
						<input type="text" class="form-control" name="state" placeholder="Provinsi" value="<?= isset($merchant_data['state']) ? $merchant_data['state'] : ''?>" style="margin-bottom: 10px">
						<input type="number" class="form-control" name="postal_code" placeholder="Kode Pos" value="<?= isset($merchant_data['postal_code']) ? $merchant_data['postal_code'] : ''?>" style="margin-bottom: 10px">
						<input type="text" class="form-control" value="Indonesia" style="margin-bottom: 10px" disabled>
						<span class="text-danger" id="invalidAddress" style="display: none;"><small>Alamat lengkap harus diisi</small></span>
					</div>
				</div>
				<div class="space" style="height: 30px"></div>
				<div class="row">
					<div class="col-12">
						<label>Zona Waktu : </label>
						<?php $time_zone = !empty($merchant_data['time_zone']) ? $merchant_data['time_zone'] : false ?>
						<select class="form-control" name="time_zone">
							<option value="7" <?= $time_zone == 7 ? 'selected' : ''?>>WIB (GMT +7)</option>
							<option value="8" <?= $time_zone == 8 ? 'selected' : ''?>>WITA (GMT +8)</option>
							<option value="9" <?= $time_zone == 9 ? 'selected' : ''?>>WIT (GMT +9)</option>
						</select>
					</div>
				</div>
				<br><br>
				<span class="text-muted">Data Login</span>
				<hr>
				<div class="row">
					<div class="col-12">
						<label>Email : </label>
						<input type="text" class="form-control" name="email" value="<?= isset($merchant_data['email']) ? $merchant_data['email'] : ''?>" disabled>
					</div>
				</div>
				<div class="space" style="height: 20px"></div>
				<div id="passwordChange" style="display: none">
					<div class="row">
						<div class="col-12">
							<label>Password Lama : </label>
							<input type="password" class="form-control" name="old_password" placeholder="Masukan password lama">
							<span class="text-danger invalid-password" style="display: none;"><small>* Wajib diisi</small></span>
						</div>
					</div>
					<div class="space" style="height: 30px"></div>
					<div class="row">
						<div class="col-12">
							<label>Password Baru : </label>
							<input type="password" class="form-control" name="new_password" placeholder="Masukan password baru">
							<span class="text-danger invalid-password" style="display: none;"><small>* Wajib diisi</small></span>
						</div>
					</div>
					<div class="space" style="height: 30px"></div>
					<div class="row">
						<div class="col-12">
							<label>Konfirmasi Password : </label>
							<input type="password" class="form-control" name="confirm_new_password" placeholder="Konfirmasi password baru">
							<span class="text-danger" id="invalid-confirm-password" style="display: none;"><small>* Password tidak sama</small></span>
						</div>
					</div>
					<div class="space" style="height: 20px"></div>
				</div>
				<div class="row">
					<div class="col-12">
						<a href="javascript:void(0)" id="passwordChangeText">Ubah Password</a>
					</div>
				</div>
				<div class="space" style="height: 20px"></div>
				<div class="row">
					<div class="col-12 text-right">
						<button type="button" id="updateSubmit" class="btn btn-outline-primary">Update</button>
					</div>
				</div>
				<div class="space" style="height: 30px"></div>
			</div>
		</div>
	</div>
	<div class="space" style="height: 60px"></div>
</div>