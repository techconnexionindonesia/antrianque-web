<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Initial extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_nofollow'] = true;
		$this->load->model('merchant_queue_option_model');
		$this->load->model('subscription_type_model');
	}

	public function index() {
		redirect('merchant/auth/login');
	}

	public function methods() {
		$this->page_criteria('active');
		if (!$this->session->flashdata('initial_merchant')){
			redirect('merchant/dashboard');
		}
		$this->data['title'] = "AntrianQue - Selamat Datang";
		$this->data['meta_nofollow'] = true;
		$this->session->set_flashdata('initial_merchant_subscription', true);
		/* Get Method Data */
		$id_merchant = $this->get_merchant_session_id();
		$methods = $this->merchant_queue_option_model->get(array('id_merchant' => $id_merchant));
		$this->data['methods'] = reset($methods);
		$this->page_display->display_merchant('dashboard/initial_methods',$this->data,true);
	}

	public function subscription() {
		$this->page_criteria('active');
		if (!$this->session->flashdata('initial_merchant_subscription')){
			redirect('merchant/dashboard');
		}
		$this->data['title'] = "AntrianQue - Pilih Paket";
		$this->data['meta_nofollow'] = true;
		$this->session->set_userdata('initial_merchant_subscription', true);
		/* Get Subscription Data */
		$this->data['subscriptions'] = $this->subscription_type_model->get();
		foreach ($this->data['subscriptions'] as $key => $value) {
			$price = $this->data['subscriptions'][$key]['price'];
			$discount = $this->data['subscriptions'][$key]['discount'];
			$discount_calculated = $price * $discount / 100;
			$price_after_discount = $price - $discount_calculated;
			$this->data['subscriptions'][$key]['price_after_discount'] = $price_after_discount;
		}
		$this->page_display->display_merchant('dashboard/initial_subscription',$this->data,true);
	}
}