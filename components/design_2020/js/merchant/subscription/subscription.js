/*
 * Javascript
 * AntrianQue Web
 * -
 * Profile Function
*/
var is_show_subscription = 0;

$(document).ready(function(){

	if ($('#toogle_subscription').length){
		$('#toogle_subscription').click(function(){
			if (!is_show_subscription){
				$('#subscription_selection').slideDown(500);
				$('#toogle_subscription_indicator').removeClass('fa-chevron-circle-down');
				$('#toogle_subscription_indicator').addClass('fa-chevron-circle-up');
				$('#toogle_subscription_text').text('Sembunyikan pilihan paket');
				is_show_subscription = 1;
			} else {
				$('#subscription_selection').slideUp(500);
				$('#toogle_subscription_indicator').removeClass('fa-chevron-circle-up');
				$('#toogle_subscription_indicator').addClass('fa-chevron-circle-down');
				$('#toogle_subscription_text').text('Tampilkan pilihan paket');
				is_show_subscription = 0;
			}
		});
	}

	if ($('#confirm_delete').length){
		$('#confirm_delete').click(function(){
			show_loading();
			setTimeout(function(){
				var result = do_ajax_post_sync(URL_AJAX_MERCHANT_SUBSCRIPTION_DELETE,{method : 'delete'},'POST','json');
				result = get_response_ajax(result, 'json');
				hide_loading();
				if (result.status == 'success'){
					show_alert_success("Paket berhasil dihapus");
					setTimeout(function(){
						location.reload();
					},3000);
				} else {
					show_alert_failed();
				}
			},100);
		});
	}

	if ($('#agreement').length){
		$('#agreement').change(function(){
			if ($('#agreement').is(':checked')){
				$('#buySubmit').prop('disabled', false);
			} else {
				$('#buySubmit').prop('disabled', true);
			}
		})
	}

	if ($('#buySubmit').length){
		$('#buySubmit').click(function(){
			show_loading();
			setTimeout(function(){
				var criteria = {id_subscription:$('#id_subscription').val()};
				if (criteria){
					var response = do_ajax_post_sync(URL_AJAX_MERCHANT_SUBSCRIPTION_BUY,criteria,'POST','json');
					response = get_response_ajax(response, 'JSON');
					if (response.status == 'success'){
						redirect(response.url);
					} else {
						show_alert_failed();
					}
				}
				hide_loading();
			},100)
		});
	}


});