<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
 
class Email_lib {
 
	public function __construct() { 
		$this->_CI = &get_instance();
	    require APPPATH.'third_party/phpmailer/Exception.php';
	    require APPPATH.'third_party/phpmailer/PHPMailer.php';
	    require APPPATH.'third_party/phpmailer/SMTP.php';    
	    $this->set_email_config();
	    $this->mail->isHTML(true);
    }

    private function set_email_config(){
	    $response = false;
	    $this->mail = new PHPMailer();
        //$this->mail->SMTPDebug  = 2;
	   
	    $this->mail->isSMTP();
	    $this->mail->Host     = $this->_CI->config->item('email_smtp');
	    $this->mail->SMTPAuth = true;
	    $this->mail->Username = $this->_CI->config->item('email_username');
	    $this->mail->Password = $this->_CI->config->item('email_password');
	    // $this->mail->SMTPSecure = 'ssl';
	    $this->mail->Port     = $this->_CI->config->item('email_port');
	    $this->mail->setFrom($this->_CI->config->item('email_username'), $this->_CI->config->item('email_name'));
	    $this->mail->addReplyTo($this->_CI->config->item('email_username'), $this->_CI->config->item('email_name'));
    }

    private function set_subject($subject){
		$this->mail->Subject = $subject;
    }

    private function set_address($email){
    	$this->mail->addAddress($email);
    }

    private function set_bcc($bcc){
    	foreach ($bcc as $email) {
    		$this->mail->AddBCC($email);
    	}
    }

    private function set_content($template,$data){
    	$content = $this->_CI->page_display->get_email_template($template, $data);
    	$this->mail->Body = $content;
    }

    private function set_data($subject, $address = false, $template = false, $data = false, $bcc = false) {
    	$this->set_subject($subject);

    	if ($address)
    		$this->set_address($address);

    	if ($template && $data)
    		$this->set_content($template, $data);

    	if ($bcc)
    		$this->set_bcc($bcc);
    }

    /* ------------------------------------------------------------- */
    /* ---------------------- Method Below ------------------------- */
    /* ------------------------------------------------------------- */

    public function do_send_feedback($data) {
    	if ($data){
    		$subject = 'AntrianQue Feedback';
    		$address = 'andrealex515@gmail.com';
    		$template = 'feedback';
    		$this->set_data($subject, $address, $template, $data);
    		return $this->mail->send();
    	} else return false;
	}

    public function do_send_verification($data) {
        if ($data){
            $subject = 'Verifikasi akun merchant AntrianQue';
            $address = $data['email'];
            $template = 'verification';
            $this->set_data($subject, $address, $template, $data);
            return $this->mail->send();
        } else return false;
    }

    public function do_send_cs_message($data) {
        if ($data){
            $subject = 'AntrianQue Support Request';
            $address = EMAIL_CS;
            $template = 'cs_message';
            $bcc = $this->_CI->config->item('customer_support_bcc');
            $this->set_data($subject, $address, $template, $data, $bcc);
            return $this->mail->send();
        } else return false;
    }

    public function do_send_subscription_payment_successful($data) {
        if ($data){
            $subject = 'Pembayaran Paket AntrianQue';
            $address = $data['email'];
            $template = 'subscription_payment_successful';
            $this->set_data($subject, $address, $template, $data);
            return $this->mail->send();
        }
    }
 
}
 