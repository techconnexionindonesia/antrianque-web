<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function trim_by_array($strings){
	foreach ($strings as $key => $value) {
		$strings[$key] = trim($value);
	}
	return $strings;
}

function random_string_builder($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function random_number_generator($length) {
    $result = '';

    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }

    return $result;
}