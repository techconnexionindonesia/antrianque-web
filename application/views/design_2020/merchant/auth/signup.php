<div class="container-fluid">
	<div class="row vh-100">
		<!-- Left / Top Section -->
		<div class="col-12 col-lg-4">
			<div class="row h-100">
				<div class="col-12 my-auto p-5">
					<div class="text-center">
						<div class="mt-5 text-center">
							<img src="<?= base_url().IMG_HTML_FOLDER?>logo/ANQ-nt-blue.png" class="mr-2" width="50" height="50">
							<span class="h2 align-middle text-tosca"><b>AntrianQue</b></span>
						</div>
						<hr>
					</div>
					<span class="h4 text-tosca"><strong><i class="fas fa-sign-in-alt mr-2 mb-3"></i>DAFTAR</strong></span><br>
					<?php echo form_open('merchant/auth/signup', array('id' => 'signupForm'))?>
					<div class="form-group">
						<!-- Signup Form 1 -->
						<div id="signupForm1">
							<!-- Email Section -->
							<span class="text-muted">Email :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-envelope"></i></span>
								</div>
								<input id="email" type="email" name="email" class="form-control" placeholder="Masukan Email" required>
							</div>
							<div id="signupFormEmailEmpty" style="display: none">
								<span class="text-danger"><small><i>* Email tidak boleh kosong</i></small></span>
							</div>
							<div id="signupFormEmailError" style="display: none">
								<span class="text-danger"><small><i>* Email ini tidak dapat digunakan untuk AntrianQue</i></small></span>
							</div>
							<!-- End of Email Section -->
							<br>
							<!-- Password Section -->
							<span class="text-muted">Password :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input id="password" type="password" name="password" class="form-control" placeholder="Masukan Password" minlength="6" required>
							</div>
							<div style="margin-bottom: -10px">
								<span class="text-muted"><small><i>* Minimal 6 karakter</i></small></span>
							</div>
							<div id="signupFormPasswordError" style="display: none">
								<span class="text-danger"><small><i>* Password tidak boleh memiliki spasi</i></small></span>
							</div>
							<div id="signupFormPasswordEmpty" style="display: none">
								<span class="text-danger"><small><i>* Password tidak boleh kosong dan minimal 6 karakter</i></small></span>
							</div>
							<!-- End of Password Section -->
							<br>
							<!-- Re-Password Section -->
							<span class="text-muted">Konfirmasi Password :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input id="repassword" type="password" class="form-control" placeholder="Konfirmasi Password" required>
							</div>
							<div id="signupFormRepasswordError" style="display: none">
								<span class="text-danger"><small><i>* Password yang anda masukan salah</i></small></span>
							</div>
							<!-- End of Re-Password Section -->
							<br>
							<div class="text-center text-muted" id="signupForm1Loading" style="display: none">
								<i class="fas fa-spinner fa-pulse mr-2"></i> Sedang memproses . . .
							</div>
							<div class="text-right">
								<button id="signupForm1Submit" type="button" class="btn btn-outline-primary w-25 mb-3"><i class="far fa-arrow-alt-circle-right"></i></button>
							</div>
						</div>

						<!-- Signup Form 2 -->
						<div id="signupForm2" style="display: none">
							<!-- Name Section -->
							<span class="text-muted">Nama Merchant :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="text" name="name" class="form-control" placeholder="Nama Merchant" required>
							</div>
							<!-- End of Name Section -->
							<br>
							<!-- Phone Section -->
							<span class="text-muted">No. Telepon :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-phone"></i></span>
								</div>
								<input type="number" name="phone" class="form-control" placeholder="Cth : 081212345678" required>
							</div>
							<!-- End of Phone Section -->
							<br>
							<!-- Description Section -->
							<span class="text-muted">Deskripsi Usaha :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fas fa-question"></i></span>
								</div>
								<textarea name="description" class="form-control" placeholder="Jelaskan tentang usaha anda" rows="2" required></textarea>
							</div>
							<!-- End of Description Section -->
							<br>
							<!-- Address Section -->
							<span class="text-muted">Alamat Usaha :</span>
							<input type="text" name="address_line1" class="form-control mb-2" placeholder="Cth : Jl. Sudirman no 12">
							<input type="text" name="address_line2" class="form-control mb-2" placeholder="Cth : Gedung Hassanudin lantai 5">
							<input type="text" name="city" class="form-control mb-2" placeholder="Kota">
							<input type="text" name="state" class="form-control mb-2" placeholder="Provinsi">
							<input type="text" name="postal_code" class="form-control" placeholder="Kode Pos">
							<!-- End of Address Section -->
							<br>
							<!-- Time Zone Section -->
							<span class="text-muted">Zona Waktu :</span>
							<div class="input-group">
								<div class="input-group-prepend">
									 <span class="input-group-text"><i class="fa fa-globe"></i></span>
								</div>
								<select name="time_zone" class="form-control" required>
									<option value="7">WIB (GMT +7)</option>
									<option value="8">WITA (GMT +8)</option>
									<option value="9">WIT (GMT + 9)</option>
								</select>
							</div>
							<!-- End of Time Zone Section -->
							<br>
							<div class="text-center text-muted mb-3" id="signupForm2Loading" style="display: none">
								<i class="fas fa-spinner fa-pulse mr-2"></i> Sedang memproses . . .
							</div>
							<div class="d-flex justify-content-between">
								<button type="button" class="btn btn-outline-primary w-25 mb-3" id="signupForm2Back"><i class="far fa-arrow-alt-circle-left"></i></button>

								<button type="submit" class="btn btn-primary w-25 mb-3">Daftar <i class="fas fa-check"></i></button>
							</div>
						</div>
					</div>
					<?php echo form_close()?>
					<div class="text-center col-12">
						<a href="<?= base_url('merchant/auth/login')?>">
							<span class="text-primary">Saya sudah punya akun AntrianQue</span>
						</a>
					</div>
					<!-- Failed login section -->
					<?php if (isset($status)){?>
					<div class="col-12 mb-3 text-center text-danger">
						<i class="fas fa-exclamation-circle mr-2"></i>
						<?php if (isset($status) && $status == 'failed') {?>
							Anda gagal login
						<?php }?>
						<?php if (isset($status) && $status == 'suspended') {?>
							Akun anda sedang disuspend. Untuk bantuan hubungi <?= EMAIL_CS ?>
						<?php }?>
						<?php if (isset($status) && $status == 'deleted') {?>
							Akun anda telah dihapus. Untuk bantuan hubungi <?= EMAIL_CS ?>
						<?php }?>
					</div>
					<?php }?>
					<!-- End of Failed login section -->
				</div>
			</div>
		</div>
		<!-- End of Left / Top Section -->
		<!-- Right Section -->
		<div class="col-12 col-lg-8 bg-tosca" id="auth-decoration">
			<div class="row h-100">
				<div class="col-12 my-auto p-5 text-light">
					<div class="d-none d-md-block">
						<span class="display-4">Lorem Ipsum Dolor Sit</span><br><br>
						<span class="h2">simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
					</div>
					<div class="d-md-none text-center">
						<span class="h2">Lorem Ipsum Dolor Sit</span><br><br>
						<span class="h5">simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
					</div>
				</div>
			</div>
		</div>
		<!-- End of right Section -->
	</div>
</div>