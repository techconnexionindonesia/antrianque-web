<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('email_lib');
	}

	public function index() {
		$this->data['title'] = "AntrianQue - Antrian Online";
		$this->data['meta_description'] = "AntrianQue adalah layanan antrian online untuk memberi kemudahan mengantri dalam bisnis anda. Daftar sekarang juga ! Gratis !";
		$this->page_display->display('landing_page',$this->data,false,true);
	}

	public function contact_form() {
		if ($this->input->post()){
			$data = $this->input->post();
			if (!empty($data['name']) && !empty($data['email']) && !empty($data['message'])){
				$data['datetime'] = date('d-m-Y H:i');
				$this->email_lib->do_send_feedback($data);
			}
		} else {
			exit('restricted');
		}
	}

}
