<img src="https://i.ibb.co/30BkSh0/Email-succes-subcsription-payment-FIX-1.png" width="100%">
<br><br>
<center>
<p style="font-size:18px">
Anda telah berhasil melakukan pembayaran untuk<br>
<b>/subscription_name/</b><br>
Di AntrianQue dengan detail berikut :
</p>
<hr><br>
<table>
<tr>
<td style="font-size: 18px"><b>Harga Bayar</b></td>
<td style="font-size: 18px">: Rp./price/</td>
</tr>  
<tr>
<td style="font-size: 18px"><b>Waktu Pembayaran</b></td>
<td style="font-size: 18px">: /paid_date/</td>
<tr>
<td style="font-size: 18px"><b>Durasi Paket</b></td>
<td style="font-size: 18px">: /duration/ hari</td>
</tr>  
</table>
<br>
<p style="font-size:18px">
Paket anda telah diaktifkan dari /subscription_start/ sampai dengan /subscription_expired/ dengan jumlah maksimal antrian per hari sebanyak :<br>
<b>/max/ antrian</b>
</p>
<hr>
<a href="www.antrianque.com" style="font-size: 18px">www.antrianque.com</a>
</center>