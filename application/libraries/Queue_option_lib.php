<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Queue_option_lib {
 
	public function __construct() { 
		$this->CI = &get_instance();
        $this->CI->load->helper('utility_helper');
        $this->CI->load->model('merchant_queue_option_model');
	    require APPPATH.'third_party/phpqrcode/qrlib.php';
    }

    public function init_option_data($id_merchant) {
        if ($id_merchant){
            $criteria = array(
                'id_merchant' => $id_merchant
            );
            $data = $this->CI->merchant_queue_option_model->get($criteria);
            $data = reset($data);
            if (!$data){
                $criteria = array(
                    'id_merchant' => $id_merchant,
                    'only_verified_user' => 0,
                    'queue_available' => 1,
                    'queue_reset' => 0
                );
                $this->CI->merchant_queue_option_model->insert($criteria);
            }
        }
    }

    public function generate_qr_code($id_merchant) {
        if ($id_merchant){
            /* Generate QR Code */
            $content = 'ANQ'.random_string_builder(QR_CONTENT_LENGTH);
            $format = 'png';
            $title = 'ANQ_qr_'.random_string_builder(QR_TITLE_LENGTH).'.'.$format;
            QRcode::png($content, IMG_FOLDER.'qr/'.$title, QR_ECLEVEL_H, 10,4);
            $this->place_logo_in_qr($title);

            /* Check if DB data was exist before */
            $criteria_id_merchant = array(
                'id_merchant' => $id_merchant
            );
            $data = $this->CI->merchant_queue_option_model->get($criteria_id_merchant);
            $data = reset($data);
            if ($data['qr_url']){
                $filename = $data['qr_url'];
                unlink(IMG_FOLDER.'qr/'.$filename);
            }

            /* Update Database */
            $criteria = array(
                'qr_url' => $title,
                'qr_content' => $content
            );
            $this->CI->merchant_queue_option_model->update($criteria, $criteria_id_merchant);
        }
    }

    public function generate_pin_number($id_merchant) {
        if ($id_merchant){
            $pin_number = random_number_generator(PIN_LENGTH);
            $criteria_id_merchant = array(
                'id_merchant' => $id_merchant
            );
            $criteria = array(
                'pin' => $pin_number
            );
            $this->CI->merchant_queue_option_model->update($criteria, $criteria_id_merchant);
        }
    }

    private function place_logo_in_qr($qr_file){
        $QR = imagecreatefrompng(IMG_FOLDER.'qr/'.$qr_file);
        $logopath = IMG_HTML_FOLDER.'logo/ANQ-blue.png';
        $logo = imagecreatefromstring(file_get_contents($logopath));
         
        imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
        imagealphablending($logo , false);
        imagesavealpha($logo , true);

        $QR_width = imagesx($QR);
        $QR_height = imagesy($QR);

        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);

        $logo_qr_width = $QR_width/4;
        $scale = $logo_width/$logo_qr_width;
        $logo_qr_height = $logo_height/$scale;

        imagecopyresampled($QR, $logo, $QR_width/2.6, $QR_height/2.6, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        imagepng($QR,IMG_FOLDER.'qr/'.$qr_file);
    }
}