<!DOCTYPE html>
<html>
    <head>
        <!-- Meta Tag -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="<?= isset($meta_description) ? $meta_description : ''?>">
        <?php
        if (isset($meta_noindex)){?>
        <meta name=”robots” content=”noindex,nofollow”>
        <?php } else {?>
        <meta name=”robots” content=”index,follow”>
        <?php }?>
        <!-- Title -->
        <title><?=$title?></title>
        <!-- Load CSS -->
        <!-- Load Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?= $component_folder.VENDOR_CSS_FOLDER.'bootstrap.min.css'?>">
        <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?= $component_folder.CSS_FOLDER.'main.css'?>">
        <link rel="stylesheet" type="text/css" href="<?= $component_folder.CSS_FOLDER.'footer-tools.css'?>">
        <!-- Load custom vendor css-->
        <?php if (!empty($custom_vendor_css)){
        foreach ($custom_vendor_css as $vendor_css){?>
        <link rel="stylesheet" type="text/css" href="<?= $component_folder.VENDOR_CSS_FOLDER.$vendor_css.'.js'?>">
        <?php }}?>
        <!-- Load custom css-->
        <?php if (!empty($custom_css)){
        foreach ($custom_css as $css){?>
        <link rel="stylesheet" type="text/css" href="<?= $component_folder.CSS_FOLDER.$css.'.css'?>">
        <?php }}?>
        <!-- end -->
    </head>
    <script type="text/javascript">
        var base_url = "<?= base_url()?>";
    </script>
    <body>
        <noscript>
            <div style="width: 100%" class="bg-danger text-light text-center py-1">
                <i class="fas fa-exclamation-circle mr-2"></i>Javascript dinonaktifkan di browser anda. Silahkan aktifkan Javascript untuk dapat menggunakan AntrianQue
            </div> 
        </noscript>