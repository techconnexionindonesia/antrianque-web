<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('merchant_lib');
		$this->load->library('validator_lib');
		$this->load->library('email_lib');
		$this->load->model('merchant_queue_option_model');
		$this->load->model('queue_numbering_model');
		$this->load->model('support_request_model');
	}

	public function index() {
		$this->page_criteria('active');
		$this->data['title'] = "AntrianQue - Dashboard";
		$this->data['meta_description'] = "Login sekarang dan kontrol antrian di tempat usaha anda menggunakan teknologi internet.";
		$this->data['merchant_data'] = $this->merchant_lib->get_merchant_data();
		$this->page_display->display_merchant('dashboard/index',$this->data);
	}

	public function set_status() {
		$result['status'] = 'failed';
		$queue_available = $this->input->post('status');
		if (isset($queue_available) && $this->is_loged_merchant()){
			$id_merchant = $this->get_merchant_session_id();
			$is_validated = $this->validator_lib->validate($queue_available, "queue_available");
			if ($is_validated){
				$is_updated = $this->merchant_queue_option_model->update(array('queue_available' => $queue_available), array('id_merchant' => $id_merchant));
				if ($is_updated){
					$result['status'] = 'success';
				}
			}
		}
		echo json_encode($result);
	}

	public function reset_numbering() {
		$result['status'] = 'failed';
		$prefix = $this->input->post('prefix');
		$start_from = $this->input->post('start_from');
		if (!is_numeric($start_from) || $start_from < 0){ // Need update for criteria not exceed max queue available
			$start_from = 1;
		}
		if ($this->is_loged_merchant()){
			$id_merchant = $this->get_merchant_session_id();
			$data_numbering = $this->queue_numbering_model->get(array('id_merchant' => $id_merchant));
			if (count($data_numbering) > 1){
				$is_updated = $this->queue_numbering_model->update(array('numbering'=>$start_from),array('id_merchant'=>$id_merchant, 'prefix'=>$prefix));
				if ($is_updated){$result['status'] = 'success';}
			} else {
				$is_updated = $this->queue_numbering_model->update(array('numbering'=>$start_from),array('id_merchant'=>$id_merchant));
				if ($is_updated){$result['status'] = 'success';}
			}
		}
		echo json_encode($result);
	}

	public function cs_submit_message() {
		$result['status'] = 'failed';
		$message = $this->input->post('message');
		if ($message && $this->is_loged_merchant()){
			$merchant = $this->merchant_lib->get_merchant_data();
			$criteria_support_request = array(
				'message' => $message,
				'id_merchant' => $merchant['id_merchant']
			);
			if ($this->support_request_model->insert($criteria_support_request)){
				$data = array(
	                'email' => $merchant['email'],
	                'id_merchant' => $merchant['id_merchant'],
	                'name' => $merchant['name'],
	                'message' => $message,
	                'datetime' => date('d-m-y H:i')
	            );
	            $this->email_lib->do_send_cs_message($data);
	            $result['status'] = 'success';
			}
		}
		echo json_encode($result);
	}
}