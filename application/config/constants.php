<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code




/*---------------------
	AntrianQue Constant
  ---------------------
*/

defined('CSS_FOLDER')					OR define('CSS_FOLDER', 'css/');
defined('JS_FOLDER')					OR define('JS_FOLDER', 'js/');
defined('VENDOR_CSS_FOLDER')			OR define('VENDOR_CSS_FOLDER', 'vendor/css/');
defined('VENDOR_JS_FOLDER')				OR define('VENDOR_JS_FOLDER', 'vendor/js/');
defined('IMG_FOLDER')					OR define('IMG_FOLDER', 'components/img/');
defined('IMG_HTML_FOLDER')				OR define('IMG_HTML_FOLDER', 'components/img_html/');
defined('AUDIO_FOLDER')					OR define('AUDIO_FOLDER', 'components/audio/');

defined('MERCHANT_STATUS_PENDING')		OR define('MERCHANT_STATUS_PENDING', 'pending');
defined('MERCHANT_STATUS_ACTIVE')		OR define('MERCHANT_STATUS_ACTIVE', 'active');
defined('MERCHANT_STATUS_PENDING')		OR define('MERCHANT_STATUS_PENDING', 'pending');
defined('QR_CONTENT_LENGTH')			OR define('QR_CONTENT_LENGTH', 64);
defined('QR_TITLE_LENGTH')				OR define('QR_TITLE_LENGTH', 20);
defined('PIN_LENGTH')					OR define('PIN_LENGTH', 6);
defined('SERVER_GMT')					OR define('SERVER_GMT', 7);
defined('SUBSCRIPTION_FREE_MAX_QUEUE')	OR define('SUBSCRIPTION_FREE_MAX_QUEUE', 500);
defined('PAYMENT_GATEWAY_GENERAL_FEE')	OR define('PAYMENT_GATEWAY_GENERAL_FEE', 4000);
defined('PAYMENT_GATEWAY_QRIS_FEE')		OR define('PAYMENT_GATEWAY_QRIS_FEE', 0.7);

defined('EMAIL_CS')						OR define('EMAIL_CS', 'cs@antrianque.com');

/*---------------------
	AntrianQue API Constant
  ---------------------
*/

if (ENVIRONMENT == 'production'){
	// define('IPAYMU_API_URL', 'https://my.ipaymu.com/'); --- still testing
	define('IPAYMU_API_URL', 'https://sandbox.ipaymu.com/'); // --- use sandbox because ipaymu account not ready yet
	define('IPAYMU_API_RESPONSE_IP', '120.89.93.249');
} else {
	define('IPAYMU_API_URL', 'https://sandbox.ipaymu.com/');
	// define('IPAYMU_API_RESPONSE_IP', '120.89.93.244');
	define('IPAYMU_API_RESPONSE_IP', '127.0.0.1');
}
define('IPAYMU_API_PAYMENT_URL', IPAYMU_API_URL.'payment');
define('IPAYMU_API_CHECK_URL', IPAYMU_API_URL.'api/transaksi');
define('STATUS_PAYMENT_SUCCESS', 'berhasil');
define('STATUS_PAYMENT_FAILED', 'gagal');

/*---------------------
	DB Constant
  ---------------------
*/

defined('MERCHANT_TABLE')						OR define('MERCHANT_TABLE', 'merchant');
defined('MERCHANT_PRIMARY_KEY')					OR define('MERCHANT_PRIMARY_KEY', 'id_merchant');
defined('MERCHANT_ACTIVITY_TABLE')				OR define('MERCHANT_ACTIVITY_TABLE', 'merchant_activity');
defined('MERCHANT_ACTIVITY_PRIMARY_KEY')		OR define('MERCHANT_ACTIVITY_PRIMARY_KEY', 'id_merchant_activity');
defined('MERCHANT_PENDING_SUBSCRIPTION_TABLE')	OR define('MERCHANT_PENDING_SUBSCRIPTION_TABLE', 'merchant_pending_subscription');
defined('MERCHANT_PENDING_SUBSCRIPTION_PRIMARY_KEY')	OR define('MERCHANT_PENDING_SUBSCRIPTION_PRIMARY_KEY', 'id_merchant_pending_subscription');
defined('MERCHANT_PROFILE_TABLE')				OR define('MERCHANT_PROFILE_TABLE', 'merchant_profile');
defined('MERCHANT_PROFILE_PRIMARY_KEY')			OR define('MERCHANT_PROFILE_PRIMARY_KEY', 'id_merchant_profile');
defined('MERCHANT_QUEUE_OPTION_TABLE')			OR define('MERCHANT_QUEUE_OPTION_TABLE', 'merchant_queue_option');
defined('MERCHANT_QUEUE_OPTION_PRIMARY_KEY')	OR define('MERCHANT_QUEUE_OPTION_PRIMARY_KEY', 'id_queue_option');
defined('MERCHANT_SUBSCRIPTION_TABLE')			OR define('MERCHANT_SUBSCRIPTION_TABLE', 'merchant_subscription');
defined('MERCHANT_SUBSCRIPTION_PRIMARY_KEY')	OR define('MERCHANT_SUBSCRIPTION_PRIMARY_KEY', 'id_merchant_subscription');
defined('QUEUE_NUMBERING_TABLE')				OR define('QUEUE_NUMBERING_TABLE', 'queue_numbering');
defined('QUEUE_NUMBERING_PRIMARY_KEY')			OR define('QUEUE_NUMBERING_PRIMARY_KEY', 'id_queue_numbering');
defined('SUBSCRIPTION_TYPE_TABLE')				OR define('SUBSCRIPTION_TYPE_TABLE', 'subscription_type');
defined('SUBSCRIPTION_TYPE_PRIMARY_KEY')		OR define('SUBSCRIPTION_TYPE_PRIMARY_KEY', 'id_subscription_type');
defined('SUPPORT_REQUEST_TABLE')				OR define('SUPPORT_REQUEST_TABLE', 'support_request');
defined('SUPPORT_REQUEST_PRIMARY_KEY')			OR define('SUPPORT_REQUEST_PRIMARY_KEY', 'id_support_request');
defined('VERIFICATION_TABLE')					OR define('VERIFICATION_TABLE', 'verification');
defined('VERIFICATION_PRIMARY_KEY')				OR define('VERIFICATION_PRIMARY_KEY', 'id_verification');