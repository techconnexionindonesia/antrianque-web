<?php

class Merchant_activity_model extends MY_Model {

    // ------------------------------------------------------------------------

    protected $_table    	= MERCHANT_ACTIVITY_TABLE;
    protected $_primary_key = MERCHANT_ACTIVITY_PRIMARY_KEY;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

}