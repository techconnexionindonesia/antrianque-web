<?php defined("BASEPATH") OR exit('No direct script allowed');

class MY_model extends CI_Model {

	protected $_table;
	protected $_primary_key;
	private $_db_mode;
	protected $_fetch_mode = 'array';

	/**
	 * Construct CI_Model
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	/**
	 * Get table of DB
	 *
	 * @return string
	 */
	public function get_table(){
		return $this->_table;
	}

	/**
	 * Get primary key of table
	 *
	 * @return string
	 */
	public function get_primary_key(){
		return $this->_primary_key;
	}

	// --------- Database get function -------- //

	/**
	 * Get Function from table
	 */
	public function get($criteria = null, $optional_value = null, $order_by = null, $limit = null, $fields = null)
	{
		if ($fields) 
            $this->db->select($fields);
                    

        if ($order_by != null) {
            if(array_keys(@$order_by) !== range(0, count(@$order_by) - 1)) {
                foreach ($order_by as $key => $value) {
                    $this->db->order_by($key,$value);
                }
            }else{
                if($order_by[1])
                    $this->db->order_by($order_by[0],'asc');
                else
                    $this->db->order_by($order_by[0],'desc'); 
            }
        }
        
        // Custom limit 
        // example.1 limit array('length' => '[value]', 'offset' => '[value]')
        // example.2 limit with no array
        if(is_array($limit)){
            if($limit[0] != null)
                $this->db->limit($limit[0], $limit[1]);
            elseif(!$limit[0] AND $limit[1])
                $this->db->limit(0, $limit[1]);
        } 
        else{

            if($limit){
                if($limit == 1) $limit = 0;
                $this->db->limit(@$this->perPage, $limit);
            }
        }
            

        // Fetch all records for a table
        if ($criteria == null) {
            $query = $this->db->get($this->_table);
        } elseif (is_array($criteria)) {
            $query = $this->db->get_where($this->_table, $criteria);
        } elseif ($criteria == 'query') {
            $query = $this->db->query($optional_value);
        } else {
            if ($optional_value == null) {
                $check_null = explode('is ', $criteria);
                if(!empty($check_null[1])) 
                    $query = $this->db->get_where($this->_table, array($criteria => null));
                else
                    $query = $this->db->get_where($this->_table, array($this->_table .".". $this->_primary_key => $criteria));
            } else {
                $query = $this->db->get_where($this->_table, array($criteria => $optional_value));
            }
        }

        if ($this->_fetch_mode == 'array') {
            return $query->result_array();
        } else {
            return $query->result();
        }
	}

	// -------- Database get function ---------- //

	/**
	 * insert into table
	 *
	 */
	public function insert($data)
	{
		$this->db->insert($this->_table, $data);
		return $this->db->insert_id();
	}

	// --------- Database update function --------- //

	/**
	 * update table value
	 *
	 */
	public function insertUpdate($data, $criteria, $optional_value = null)
    {
        // First check to see if the field exists
        $this->db->select($this->_primary_key);

        if ($optional_value == null) {
            if(is_array($criteria)){
                $query = $this->db->get_where($this->_table, $criteria);
            }else{            
                $query = $this->db->get_where($this->_table, array($this->_primary_key => $criteria));
            }
        } else {
            $query = $this->db->get_where($this->_table, array($criteria => $optional_value));
        }

        // Count how many records exist with this ID
        $result = $query->num_rows();

        // INSERT
        if ($result == 0)
        {
            $this->db->insert($this->_table, $data);
            return $this->db->insert_id();
        }

        // UPDATE
        if ($optional_value == null) {
            if(is_array($criteria)){
                $this->db->where($criteria);
            }else{
                $this->db->where($this->_primary_key, $criteria);
            }
        } else {
            $this->db->where($criteria, $optional_value);
        }

        return $this->db->update($this->_table, $data);
    }

    /**
     * Update table
     *
     */
    public function update($data, $criteria, $optional_value = null)
    {
        if ($optional_value == null) {
            if (is_array($criteria)) {
                $this->db->where($criteria);
            } else {
                $this->db->where(array($this->_primary_key => $criteria));
            }
        } else {
            $this->db->where(array($criteria => $optional_value));
        }

        return $this->db->update($this->_table, $data);
    }

    

    // -------- Database delete function -------- //

    /**
     * delete table value
     *
     */
    public function delete($criteria, $optional_value = null)
    {
    	if ($optional_value == null) {
            if (is_array($criteria)) {
                $this->db->where($criteria);
            } else {
                $this->db->where(array($this->_primary_key => $criteria));
            }
        } else {
            $this->db->where($criteria, $optional_value);
        }

        $this->db->delete($this->_table);
        return $this->db->affected_rows();
    }

    // --------- Get additional method ----------- //

    
}