<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class Verification_lib {
 
	public function __construct() { 
		$this->CI = &get_instance();
        $this->CI->load->model('verification_model');
        $this->CI->load->helper('utility_helper');
        $this->id_merchant = false;
    }

    /* ---------- Primary function to determine which function to be use by id verification ------------- */

    public function verify_by_token($token = false) {
        if ($token){
            $criteria = array(
                'token' => $token
            );
            $data = $this->CI->verification_model->get($criteria);
            $data = reset($data);
            if ($data){
                $this->CI->verification_model->delete($criteria);
                if ($data['id_merchant_or_user']){
                    $this->id_merchant = $data['id_merchant_or_user'];
                }
                $method = $data['method'];
                $this->determine_method($method);
            } else {
                redirect('merchant/auth/login');
            }
        }
    }

    private function determine_method($method) {
        if ($method){
            switch ($method) {
                case 'initial_merchant':
                    $this->initial_merchant();
                    break;
            }
        }
    }

    /* ------------------------------------------------------------------------------------------------- */

    public function create_initial_verification($id_merchant = false) {
        $token = random_string_builder(32);
        if ($id_merchant){
            $criteria = array(
                'id_merchant_or_user' => $id_merchant
            );
            $data = $this->CI->verification_model->get($criteria);
            $data = reset($data);
            $method = 'initial_merchant';
            if ($data){
                $criteria = array(
                    'token' => $token
                );
                $this->CI->verification_model->update($criteria, $data['id_verification']);
            } else {
                $criteria = array(
                    'token' => $token,
                    'id_merchant_or_user' => $id_merchant,
                    'method' => $method
                );
                $this->CI->verification_model->insert($criteria);
            }
        }
        return $token;
    }

    /* ----------------------------- Verification methods ---------------------------------------- */

    private function initial_merchant() {
        $this->CI->load->library('merchant_lib');
        $this->CI->load->library('queue_option_lib');
        $this->CI->load->library('authenticator');
        if ($this->id_merchant){
            $data = $this->CI->merchant_lib->get_merchant_data($this->id_merchant);
            if ($data){
                $this->CI->queue_option_lib->init_option_data($this->id_merchant);
                $this->CI->queue_option_lib->generate_qr_code($this->id_merchant);
                $this->CI->queue_option_lib->generate_pin_number($this->id_merchant);
                $this->CI->merchant_lib->update_status($this->id_merchant, MERCHANT_STATUS_ACTIVE);
            }
            $this->CI->authenticator->set_session_merchant($this->id_merchant);
            $this->CI->merchant_activity_logger->log('Verifikasi Email');
            $this->CI->session->set_flashdata('initial_merchant', true);
            redirect('merchant/dashboard/initial/methods');
        }
    }
 
}
 