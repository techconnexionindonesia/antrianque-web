/*
 * Javascript
 * AntrianQue Web
 * -
 * Profile Function
*/

$(document).ready(function(){

	$('#updateSubmit').click(function(){
		do_update_profile();
	});

	$('#passwordChangeText').click(function(){
		toogle_password_change();
	});

	$('input, textarea').keypress(function(){
		reset_invalid_form();
	});

	function toogle_password_change() {
		if ($('#passwordChange').is(":hidden")){
			$('#passwordChangeText').text('Sembunyikan Ganti Password');
			$('#passwordChange').show(500);
		} else {
			$('input[name="old_password"]').val('');
			$('input[name="new_password"]').val('');
			$('input[name="confirm_new_password"]').val('');
			$('#passwordChangeText').text('Ubah Password');
			$('#passwordChange').hide(500);
		}
	}

	function do_update_profile() {
		var criteria = get_criteria();
		var validated = validate_criteria(criteria);
		if (validated){
			var update = do_ajax_post_sync(URL_AJAX_MERCHANT_PROFILE_UPDATE,criteria,'POST','json');
			update = get_response_ajax(update, 'JSON');
			if (update.status == 'success'){
				show_alert_success();
				setTimeout(function(){
					redirect(update.redirect);
				},3500);
			} else if (update.status == 'wrong_password'){
				show_alert_failed('Password yang anda masukan salah');
			} else {
				show_alert_failed();
			}
		}
	}

	function get_criteria() {
		var data = {
			name : get_val_by_name('name'),
			description : get_val_by_name('description','textarea'),
			address_line1 : get_val_by_name('address_line1'),
			address_line2 : get_val_by_name('address_line2'),
			city : get_val_by_name('city'),
			state : get_val_by_name('state'),
			postal_code : get_val_by_name('postal_code'),
			time_zone : get_val_by_name('time_zone')
		}
		if (get_val_by_name('old_password') && get_val_by_name('new_password') && get_val_by_name('confirm_new_password')){
			data['old_password'] = get_val_by_name('old_password');
			data['new_password'] = get_val_by_name('new_password');
			data['confirm_new_password'] = get_val_by_name('confirm_new_password');
		}
		return data;
	}

	function validate_criteria(data) {
		var validated = true;

		// Validate Name
		if (!data.name){
			validated = false;
			$('#invalidName').show(500);
		}

		// Validate Description
		if (!data.description){
			validated = false;
			$('#invalidDescription').show(500);
		}

		// Validate address
		if (
			data.address_line1 == false
			|| data.city == false
			|| data.state == false
			|| data.postal_code == false
		) {
			validated = false;
			$('#invalidAddress').show(500);
		}

		// Validate Password
		if ($('#passwordChange').is(':visible') && (
			get_val_by_name('old_password') == false
			|| get_val_by_name('new_password') == false
			|| get_val_by_name('confirm_new_password') == false			
		)){
			validated = false;
			$('.invalid-password').show(500);
		}

		if ($('#passwordChange').is(':visible') && get_val_by_name('new_password') != get_val_by_name('confirm_new_password')){
			validated = false;
			$('#invalid-confirm-password').show(500);
		}
		return validated;
	}

	function reset_invalid_form(){
		hide_all(['#invalidName','#invalidDescription','#invalidAddress','.invalid-password','#invalid-confirm-password']);
	}


});